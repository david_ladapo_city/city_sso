package uk.co.city.core.walmart.component

import android.util.Base64

import java.io.UnsupportedEncodingException
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException

import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class CryptoUtil {

    private var cipher: Cipher? = null
    private var keySpec: SecretKeySpec? = null
    private var ivSpec: IvParameterSpec? = null

    private val iv = byteArrayOf(34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34)
    // private byte[] key = {-26, 76, -81, 24, 93, -83, 8, -10, -50, -90, 8, -77, 83, 19, 121, 94, -118, -25, 45, -79, 49, 30, -37, -82, -97, -64, 89, -32, -112, -43, 104, 22};
    private val key = byteArrayOf(
        -112,
        2,
        9,
        -57,
        -78,
        -56,
        -49,
        108,
        -56,
        42,
        100,
        49,
        95,
        18,
        -110,
        -121,
        113,
        -57,
        -90,
        -82,
        -60,
        63,
        -82,
        62,
        -13,
        82,
        -39,
        37,
        12,
        80,
        100,
        45
    )

    @Throws(NoSuchAlgorithmException::class, NoSuchPaddingException::class)
    constructor() {

        this.cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        this.keySpec = SecretKeySpec(key, "AES")
        this.ivSpec = IvParameterSpec(iv)
    }

    @Throws(NoSuchAlgorithmException::class, NoSuchPaddingException::class)
    constructor(key: String) {
        this.cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        this.keySpec = SecretKeySpec(Base64.decode(key.toByteArray(), Base64.DEFAULT), "AES")
        this.ivSpec = IvParameterSpec(iv)
    }


    private val utf = "UTF-8"

    @Throws(
        InvalidKeyException::class,
        InvalidAlgorithmParameterException::class,
        IllegalBlockSizeException::class,
        BadPaddingException::class,
        UnsupportedEncodingException::class
    )
    fun encrypt(text: String): String {
        var encrypted = ""

        cipher!!.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec)

        val results = cipher!!.doFinal(text.toByteArray(charset(utf)))
        val cipherArray = ByteArray(iv.size + results.size)

        System.arraycopy(iv, 0, cipherArray, 0, iv.size)
        System.arraycopy(results, 0, cipherArray, iv.size, results.size)

        encrypted = Base64.encodeToString(cipherArray, Base64.DEFAULT)

        return encrypted
    }

    @Throws(
        InvalidKeyException::class,
        InvalidAlgorithmParameterException::class,
        UnsupportedEncodingException::class,
        IllegalBlockSizeException::class,
        BadPaddingException::class
    )
    fun decrypt(cipherText: String): String {

        var decrypted = ""

        cipher!!.init(Cipher.DECRYPT_MODE, keySpec, ivSpec)

        val decodedValue = Base64.decode(cipherText.toByteArray(charset(utf)), Base64.DEFAULT)

        val decryptedVal = cipher!!.doFinal(decodedValue)

        val actualText = ByteArray(decryptedVal.size - 16)

        System.arraycopy(decryptedVal, 16, actualText, 0, actualText.size)

        decrypted = String(actualText, charset(utf))

        return decrypted
    }
}
