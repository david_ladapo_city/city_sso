package uk.co.city.core.walmart.wmsso

object SSOConstants {
    val ACCT_TYPE = "account_type"
    val AUTH_TYPE = "auth_type"
    val ACCT_NAME = "account_name"

    val WM_ACCT_TYPE = "com.com.walmart.auth"
    val WM_AUTH_TOKEN_TYPE = "general-purpose"
    val WM_AUTH_TOKEN = "wmAuthToken"

    val LAST_LOGIN_TIME = "lastLoginTime"

    val STORE_DOMAIN = "store"
    val DC_DOMAIN = "dc"

    val EXTRA_CHANGED_FIELDS = "com.com.walmart.sso.CHANGED_FIELDS"

    val KEY_FROM_ACTIVITY = "com.com.walmart.sso.FROM_ACTIVITY"
    val KEY_LOGIN_REQUIRED = "com.com.walmart.sso.LOGIN_REQUIRED"
    val KEY_DISPLAY_LOGIN = "com.com.walmart.sso.DISPLAY_LOGIN"
    val KEY_REPLY_TO = "com.com.walmart.sso.REPLY_TO"

    val PERM_RECV_USER_CHANGED = "com.com.walmart.sso.RECV_USER_CHANGED"

    enum class Requests private constructor(var `val`: Int) {
        Unknown(-1),
        GetUser(1),
        SignOutUser(2);


        companion object {

            fun byVal(`val`: Int): Requests {
                for (r in Requests.values()) {
                    if (r.`val` == `val`) {
                        return r
                    }
                }
                return Unknown
            }
        }

    }
}
