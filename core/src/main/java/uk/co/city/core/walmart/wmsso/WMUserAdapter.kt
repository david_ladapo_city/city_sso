package uk.co.city.core.walmart.wmsso

import android.accounts.Account
import android.accounts.AccountManager
import android.os.Bundle
import android.util.Log

import org.json.JSONException
import org.json.JSONObject

/**
 * Created by awhelms on 10/9/15.
 */
object WMUserAdapter {

    private val TAG = "WMUserAdapter"

    val USERID = "userid"
    val TOKEN = "token"
    val REFRESH_TOKEN = "refreshToken"
    val ACCT_TYPE = "acctType"
    val DOMAIN = "domain"
    val SUB_DOMAIN = "subDomain"
    val SITE = "siteNbr"
    val COUNTRY = "countryCode"
    val VALIDITY = "validity"
    val AUTH_STATE = "authenticationState"
    val AUTH_RESULT_MSG = "authenticationResultMsg"
    val EXPIRES_AT = "expiresAt"
    val ADDITIONAL = "additional"
    val ENV = "env"

    fun fromBundle(bundle: Bundle): WMUser? {
        if (bundle.containsKey(USERID)) {
            val user = WMUser()
            fromBundle(bundle, user)
            return user
        } else {
            return null
        }
    }

    fun fromBundle(bundle: Bundle, user: WMUser) {

        user.userId = bundle.getString(USERID)
        user.token = bundle.getString(TOKEN)
        user.refreshToken = bundle.getString(REFRESH_TOKEN)
        user.domain = bundle.getString(DOMAIN)
        user.subDomain = bundle.getString(SUB_DOMAIN)
        user.env = bundle.getString(ENV)

        if (bundle.getLong(SITE) > 0) {
            user.siteId = bundle.getLong(SITE)
        } else {
            user.siteId = null
        }
        user.countryCode = bundle.getString(COUNTRY)
        user.tokenValidity = bundle.getLong(VALIDITY)
        user.authenticationResultMsg = bundle.getString(AUTH_RESULT_MSG)
        user.authenticationState = bundle.getString(AUTH_STATE)
        user.expiresAt = bundle.getLong(EXPIRES_AT)

        val additionalStr = bundle.getString(ADDITIONAL)
        if (additionalStr != null) {
            try {
                user.additional = JSONObject(additionalStr)
            } catch (e: JSONException) {
                Log.e(TAG, "Error parsing 'additional' field from bundle: $additionalStr", e)
            }

        }

    }

    fun toBundle(user: WMUser?): Bundle {
        val bundle = Bundle()
        if (null != user) {
            bundle.putString(USERID, user.userId)
            bundle.putString(TOKEN, user.token)
            bundle.putString(REFRESH_TOKEN, user.refreshToken)
            bundle.putString(DOMAIN, user.domain)
            bundle.putString(SUB_DOMAIN, user.subDomain)
            bundle.putString(COUNTRY, user.countryCode)

            if (user.siteId != null) {
                bundle.putLong(SITE, user.siteId!!)
            } else {
                bundle.remove(SITE)
            }

            if (user.tokenValidity != null) {
                bundle.putLong(VALIDITY, user.tokenValidity!!)
            }

            if (user.authenticationState != null) {
                bundle.putString(AUTH_STATE, user.authenticationState)
            }

            if (user.authenticationResultMsg != null) {
                bundle.putString(AUTH_RESULT_MSG, user.authenticationResultMsg)
            }
            if (user.expiresAt != null) {
                bundle.putLong(EXPIRES_AT, user.expiresAt!!)
            }

            if (user.additional != null) {
                bundle.putString(ADDITIONAL, user.additional!!.toString())
            }
        }
        return bundle
    }

    fun fromAccount(acctMgr: AccountManager, userAccount: Account, user: WMUser) {
        // get saved user data from account manager
        user.userId = userAccount.name
        user.countryCode = acctMgr.getUserData(userAccount, COUNTRY)
        user.domain = acctMgr.getUserData(userAccount, DOMAIN)
        user.subDomain = acctMgr.getUserData(userAccount, SUB_DOMAIN)
        user.refreshToken = acctMgr.getUserData(userAccount, REFRESH_TOKEN)

        try {
            user.siteId = java.lang.Long.valueOf(acctMgr.getUserData(userAccount, SITE))
        } catch (nfe: NumberFormatException) {
            user.siteId = null
        }

        try {
            user.tokenValidity = java.lang.Long.valueOf(acctMgr.getUserData(userAccount, VALIDITY))
        } catch (nfe: NumberFormatException) {
            // TODO - maybe provide a reasonable default?
            user.tokenValidity = null
        }

        try {
            user.expiresAt = java.lang.Long.valueOf(acctMgr.getUserData(userAccount, EXPIRES_AT))
        } catch (nfe: NumberFormatException) {
            user.expiresAt = null
        }

        var additionalStr: String? = ""
        try {
            additionalStr = acctMgr.getUserData(userAccount, ADDITIONAL)
            if (additionalStr != null) {
                user.additional = JSONObject(additionalStr)
            }
        } catch (e: JSONException) {
            Log.e(TAG, "Error parsing account's 'additional' field: " + additionalStr!!, e)
        }

    }

    fun fromAccount(acctMgr: AccountManager, userAccount: Account): WMUser {
        val user = WMUser()
        fromAccount(acctMgr, userAccount, user)
        return user
    }

    fun toJSON(user: WMUser): JSONObject {
        val json = JSONObject()
        try {
            json.put("userId", user.userId)
            json.put("token", user.token)
            json.put("countryCode", user.countryCode)
            json.put("domain", user.domain)

            user.siteId?.let {siteId->
                if (siteId > 0) {
                    json.put("siteId", siteId)
                }
            }

            if ("dc".equals(user.domain!!, ignoreCase = true)) {
                json.put("subDomain", user.subDomain)
            }
            user.additional?.let { additional->
                json.put("additional", additional)
            }
        } catch (e: Exception) {
            Log.e(TAG, "Error converting user to json", e)
        }

        return json
    }
}
