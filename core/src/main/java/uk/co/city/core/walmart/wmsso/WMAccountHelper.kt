package uk.co.city.core.walmart.wmsso

import android.accounts.Account
import android.accounts.AccountManager
import android.accounts.AccountManagerCallback
import android.accounts.AccountManagerFuture
import android.accounts.AuthenticatorException
import android.accounts.OperationCanceledException

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log

import androidx.localbroadcastmanager.content.LocalBroadcastManager
import uk.co.city.core.walmart.component.CryptoUtil

import java.io.IOException
import java.util.ArrayList
import java.util.concurrent.TimeUnit

/**
 *  10/6/15.
 */
class WMAccountHelper(private val ctx: Context) {
    private val acctMgr: AccountManager

    val currentWMAccount: Account?
        get() {

            val wmAccounts = acctMgr.getAccountsByType(SSOConstants.WM_ACCT_TYPE)

            for (acct in wmAccounts) {
                if (null != acctMgr.peekAuthToken(acct, SSOConstants.WM_AUTH_TOKEN_TYPE)) {
                    return acct
                }
            }

            return null
        }

    val mostRecentUser: Account?
        get() {
            val wmAccounts = acctMgr.getAccountsByType(SSOConstants.WM_ACCT_TYPE)
            var mostRecent: Account? = null
            var mostRecentLogin: Long? = 0L
            var lastLogin: Long? = 0L

            for (acct in wmAccounts) {

                if (mostRecent != null) {
                    lastLogin = getLastLogin(acct)
                    if (lastLogin!! > mostRecentLogin!!) {
                        mostRecent = acct
                        mostRecentLogin = lastLogin
                    }
                } else {
                    mostRecent = acct
                    mostRecentLogin = getLastLogin(mostRecent)
                }
            }

            return mostRecent
        }

    init {

        acctMgr = ctx.getSystemService(Context.ACCOUNT_SERVICE) as AccountManager //AccountManager.get(ctx);
    }

    fun getCurrentUser(activity: Activity, callback: AccountManagerCallback<WMUser>) {
        val user = WMUser()
        val userAccount = this.currentWMAccount

        val userFuture = object : AccountManagerFuture<WMUser> {
            private var cancelled = false

            override fun cancel(mayInterruptIfRunning: Boolean): Boolean {
                cancelled = true
                return false
            }

            override fun isCancelled(): Boolean {
                return cancelled
            }

            override fun isDone(): Boolean {
                return true
            }

            @Throws(OperationCanceledException::class, IOException::class, AuthenticatorException::class)
            override fun getResult(): WMUser {
                return user
            }

            @Throws(OperationCanceledException::class, IOException::class, AuthenticatorException::class)
            override fun getResult(timeout: Long, unit: TimeUnit): WMUser {
                return user
            }
        }


        val internalCallback = AccountManagerCallback<Bundle> { future ->
            try {
                if (!future.isCancelled) {
                    val result = future.result

                    user.userId = result.getString(AccountManager.KEY_ACCOUNT_NAME)

                    if (null != userAccount) {
                        val authToken = CryptoUtil().decrypt(result.getString(AccountManager.KEY_AUTHTOKEN))
                        user.token = authToken
                        if (result.containsKey(WMUserAdapter.USERID)) {
                            WMUserAdapter.fromBundle(result, user)
                        } else {
                            WMUserAdapter.fromAccount(acctMgr, userAccount, user)
                            Log.d(TAG, "existing user updated $user")
                        }
                        checkUserChangedAndNotify(user)
                    } else {
                        user.token = result.getString(SSOConstants.WM_AUTH_TOKEN)
                        WMUserAdapter.fromBundle(result, user)
                        checkUserChangedAndNotify(user)
                    }

                    userNotify(SSOIntentType.UserSignedIn, user)

                    callback.run(userFuture)
                } else {
                    userFuture.cancel(true)
                    callback.run(userFuture)
                }
            } catch (e: Exception) {
                Log.e(TAG, "error in the internalCallback", e)
                userFuture.cancel(true)
                callback.run(userFuture)
            }
        }


        if (null != userAccount) {
            Log.d(TAG, "account exists")
            var expiresAt: Long? = 0L
            try {
                expiresAt = java.lang.Long.parseLong(acctMgr.getUserData(userAccount, WMUserAdapter.EXPIRES_AT))
            } catch (nfe: NumberFormatException) {
                Log.e(TAG, "error getting expiration time for account " + userAccount.name, nfe)
            }

            if (expiresAt!! > 0 && expiresAt <= System.currentTimeMillis()) {
                Log.d(TAG, "expired token; invalidating in cache")
                acctMgr.invalidateAuthToken(
                    SSOConstants.WM_ACCT_TYPE,
                    acctMgr.peekAuthToken(userAccount, SSOConstants.WM_AUTH_TOKEN_TYPE)
                )
            }

            Log.d(TAG, "calling acctMgr getAuthToken")
            acctMgr.getAuthToken(userAccount, SSOConstants.WM_AUTH_TOKEN_TYPE, null, activity, internalCallback, null)
        } else {
            Log.d(TAG, "calling acctMgr addAccount")
            acctMgr.addAccount(
                SSOConstants.WM_ACCT_TYPE,
                SSOConstants.WM_AUTH_TOKEN_TYPE,
                null,
                null,
                activity,
                internalCallback,
                null
            )
        }
    }

    fun signOutUser(user: WMUser?) {
        if (user != null) {
            var encryptedToken = ""
            try {
                encryptedToken = user.token?.let { CryptoUtil().encrypt(it) }!!
            } catch (e: Exception) {
                Log.e(TAG, "Could not encrypt token for invalidation", e)
            }

            Log.d(TAG, "signing out user " + user.userId + "  with token " + encryptedToken)
            acctMgr.invalidateAuthToken(SSOConstants.WM_ACCT_TYPE, encryptedToken)

            userNotify(SSOIntentType.UserSignedOut, user)
        }
    }

    private fun getLastLogin(acct: Account?): Long? {
        var lastLogin: Long? = 0L
        try {
            val loginTime = acctMgr.getUserData(acct, SSOConstants.LAST_LOGIN_TIME)
            lastLogin = java.lang.Long.parseLong(loginTime)
        } catch (nfe: NumberFormatException) {
        }

        return lastLogin
    }

    fun getWMAccountByUserId(userId: String): Account? {
        val wmAccounts = acctMgr.getAccountsByType(SSOConstants.WM_ACCT_TYPE)

        for (acct in wmAccounts) {
            if (acct.name.equals(userId, ignoreCase = true)) {
                return acct
            }
        }

        return null
    }

    private fun userNotify(type: SSOIntentType, user: WMUser) {
        Log.d(TAG, "About to send broadcast with action " + type.action)
        val i = Intent(type.action)
        i.putExtras(WMUserAdapter.toBundle(user))
        LocalBroadcastManager.getInstance(ctx).sendBroadcast(i)
    }

    private fun checkUserChangedAndNotify(updated: WMUser) {
        val changedFields = ArrayList<String>()

        if (orig != null) {
            synchronized(orig!!) {
                Log.d(TAG, "comparing user entries.")
                Log.d(TAG, "original " + orig!!)
                Log.d(TAG, "updated $updated")
                if (orig!!.userId != updated.userId) {
                    changedFields.add(WMUserAdapter.USERID)
                }

                if (orig!!.domain != updated.domain) {
                    changedFields.add(WMUserAdapter.DOMAIN)
                } else if (updated.domain == "DC") {
                    if (orig!!.subDomain != updated.subDomain) {
                        changedFields.add(WMUserAdapter.SUB_DOMAIN)
                    }

                    if (orig!!.siteId != updated.siteId) {
                        changedFields.add(WMUserAdapter.SITE)
                    }
                } else if (updated.domain == "STORE") {
                    if (orig!!.siteId != updated.siteId) {
                        changedFields.add(WMUserAdapter.SITE)
                    }
                }

                if (orig!!.token == null || orig!!.token != updated.token) {
                    changedFields.add(WMUserAdapter.TOKEN)
                }
            }
        }


        if (orig == null || changedFields.size > 0) {
            val i = Intent(SSOIntentType.UserChanged.action)
            i.putExtras(WMUserAdapter.toBundle(updated))
            i.putStringArrayListExtra(SSOConstants.EXTRA_CHANGED_FIELDS, changedFields)

            Log.d(TAG, "Notifying user changed " + SSOIntentType.UserChanged.action)
            LocalBroadcastManager.getInstance(ctx).sendBroadcast(i)
        }

        orig = updated
    }

    companion object {
        private val TAG = "WMAccountHelper"

        private var orig: WMUser? = null
    }
}
