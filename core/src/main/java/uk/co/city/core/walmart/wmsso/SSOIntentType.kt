package uk.co.city.core.walmart.wmsso

/**
 * 12/18/15.
 */
enum class SSOIntentType constructor(val action: String) {

    UserSignedIn("com.com.walmart.sso.UserSignedIn"),
    UserSignedOut("com.com.walmart.sso.UserSignedOut"),
    UserChanged("com.com.walmart.sso.UserChanged"),
    Unknown("");


    companion object {

        fun getByAction(action: String): SSOIntentType {
            for (t in SSOIntentType.values()) {
                if (t.action == action) {
                    return t
                }
            }
            return Unknown
        }
    }
}
