package uk.co.city.core.walmart.wmsso

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.util.Log

import java.util.ArrayList

/**
 * Created by awhelms on 9/22/16.
 */
class WMAccountManagerServiceClient(private val context: Context) {

    var appEnv = "PROD"
    var hexColor: String? = null

    lateinit var isBiometricAllowed: String
    private var bound = false
    private var requestMessenger: Messenger? = null

    private val packageName: String


    private val messageQueue = ArrayList<RequestWrapper>()

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            Log.d(TAG, "Service connected")
            requestMessenger = Messenger(service)
            bound = true
            try {
                service.linkToDeath({
                    Log.d(TAG, "Service died")
                    bound = false
                }, 0)
            } catch (e: RemoteException) {
                Log.e(TAG, "Unable to register to receive death notifications for service binder")
            }

            Log.d(TAG, "invoking")
            invokeService()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            Log.d(TAG, "Service disconnected")
            requestMessenger = null
            bound = false
        }
    }

    val isServiceAvailable: Boolean
        get() {
            val pm = context.packageManager

            try {
                val info = pm.getPackageInfo(SRVC_PKG, PackageManager.GET_SERVICES)
                for (serviceInfo in info.services) {
                    if (SRVC_CLASS.equals(serviceInfo.name, ignoreCase = true)) {
                        return true
                    }
                }
            } catch (e: PackageManager.NameNotFoundException) {
            }

            return false
        }

    init {
        packageName = this.context.packageName
    }

    fun getUser(fromActivity: Activity, callback: WMAccountManagerCallback) {
        getUser(fromActivity, true, callback)
    }

    fun getUser(fromActivity: Activity, displayLoginScreenIfNoUser: Boolean, callback: WMAccountManagerCallback?) {
        if (isServiceAvailable) {
            val requestMessage = Message.obtain()
            val bundle = Bundle()
            bundle.putBoolean(SSOConstants.KEY_DISPLAY_LOGIN, displayLoginScreenIfNoUser)
            bundle.putString("ENV", appEnv)
            bundle.putString("package", packageName)
            bundle.putString("themeHexColor", hexColor)

            Log.d(TAG, "FROM LIB: $isBiometricAllowed")
            bundle.putString("isBiometricAllowed", isBiometricAllowed)

            requestMessage.what = SSOConstants.Requests.GetUser.`val`
            requestMessage.data = bundle

            val r = queueMessage(requestMessage, callback)

            requestMessage.replyTo = Messenger(GetUserResponseHandler(fromActivity, r))

            bindToService(callback)
        } else {
            callback?.onError(Exception("Service not available"))
        }
    }

    fun setEnv(env: String) {
        appEnv = env
    }

    fun setThemeColor(hexC: String) {
        hexColor = hexC
    }

    fun allowBiometric(isAllowed: String) {
        this.isBiometricAllowed = isAllowed
    }

    lateinit var successHandler: Handler
    fun signOutUser(callback: WMAccountManagerCallback) {
        if (isServiceAvailable) {
            val requestMessage = Message.obtain()
            requestMessage.what = SSOConstants.Requests.SignOutUser.`val`
            val r = queueMessage(requestMessage, callback)
            successHandler = object : Handler() {
                override fun handleMessage(msg: Message) {
                    super.handleMessage(msg)

                    callback.onSuccess(null)
                    removeFromQueue(r)
                }
            }
            requestMessage.replyTo = Messenger(successHandler)
            bindToService(callback)
        } else {
            callback?.onError(Exception("Service not available"))
        }
    }

    private fun bindToService(callback: WMAccountManagerCallback?) {
        if (!bound) {
            val i = Intent()
            i.setClassName(SRVC_PKG, SRVC_CLASS)

            try {
                context.bindService(i, connection, Context.BIND_AUTO_CREATE)
            } catch (e: SecurityException) {
                Log.e(TAG, "Caught SecurityException while calling service; app signed with proper keystore?")
                callback?.onError(
                    Exception(
                        "This application and the service it wants to communicate with must be signed with the same keystore.",
                        e
                    )
                )
            }

        } else {
            invokeService()
        }
    }

    fun unbindFromService() {
        if (connection != null) {
            Log.d(TAG, "disconnecting from service")
            try {
                this.context.unbindService(connection)
            } catch (e: IllegalArgumentException) {
            }

        }
    }

    private fun invokeService() {
        for (r in messageQueue) {
            try {
                requestMessenger!!.send(r.requestMessage)
            } catch (e: RemoteException) {
                Log.e(TAG, "Error sending request to servcie", e)
                if (r.callback != null) {
                    r.callback!!.onError(e)
                }
                disconnect()
            }

        }
    }

    private fun queueMessage(requestMessage: Message, callback: WMAccountManagerCallback?): RequestWrapper {
        synchronized(messageQueue) {
            val r = RequestWrapper(requestMessage, callback)
            messageQueue.add(r)
            return r
        }
    }

    private fun removeFromQueue(r: RequestWrapper) {
        synchronized(messageQueue) {
            messageQueue.remove(r)
            if (messageQueue.size == 0) {
                disconnect()
            }
        }
    }

    private fun disconnect() {
        if (connection != null) {
            Log.d(TAG, "disconnecting from service")
            try {
                this.context.unbindService(connection)
            } catch (e: IllegalArgumentException) {
            }

        }
    }

    private inner class RequestWrapper internal constructor(
        var requestMessage: Message,
        var callback: WMAccountManagerCallback?
    )

    private inner class GetUserResponseHandler internal constructor(
        private val fromActivity: Activity?,
        private val r: RequestWrapper
    ) : Handler() {

        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            if (msg != null && msg.data != null) {
                val bundle = msg.data
                if (bundle.containsKey(SSOConstants.KEY_LOGIN_REQUIRED)) {
                    r.requestMessage.replyTo = Messenger(GetUserResponseHandler(fromActivity, r))
                    val displayLoginPresent = bundle.containsKey(SSOConstants.KEY_DISPLAY_LOGIN)
                    val displayLogin = bundle.getBoolean(SSOConstants.KEY_DISPLAY_LOGIN)
                    if (!displayLoginPresent || displayLogin) {
                        // need to prompt the user
                        val i = Intent()
                        i.setClassName(SRVC_PKG, ACTIVITY_CLASS)
                        i.putExtra(SSOConstants.KEY_REPLY_TO, r.requestMessage.replyTo)
                        fromActivity?.startActivity(i)
                    } else {
                        r.callback!!.onSuccess(null)
                    }
                } else {
                    r.callback?.let {
                        it.onSuccess(WMUserAdapter.fromBundle(bundle)!!)
                    }
                    removeFromQueue(r)
                }
            } else {
                removeFromQueue(r)
            }
        }
    }

    companion object {
        private val TAG = "WMAcctMgrSrvcClient"

        private val BASE_PKG = "com.com.walmart.sso"
        private val SRVC_PKG = "$BASE_PKG.app"
        private val SRVC_CLASS = "$BASE_PKG.WMAccountManagerService"
        private val ACTIVITY_CLASS = "$BASE_PKG.WMLoginActivity"
    }
}
