package uk.co.city.core.walmart.wmsso

/**
 * Created by awhelms on 9/22/16.
 */
interface WMAccountManagerCallback {
    fun onSuccess(user: WMUser?)
    fun onError(t: Throwable)
}
