package uk.co.city.core.walmart.wmsso

import org.json.JSONObject

/**
 */
class WMUser {
    var userId: String? = null
        set(userId) {
            field = userId?.trim { it <= ' ' }
        }
    var token: String? = null
    var domain: String? = null
    var subDomain: String? = null
    var countryCode: String? = null
    var env: String? = null
    var siteId: Long? = null
    var refreshToken: String? = null
    var tokenValidity: Long? = null
    // probably not the right place for these
    var authenticationState: String? = null
    var authenticationResultMsg: String? = null

    var additional: JSONObject? = null

    var expiresAt: Long? = null

    override fun toString(): String {
        return "WMUser{" +
                "userId='" + this.userId + '\''.toString() +
                ", token='" + token + '\''.toString() +
                ", siteId=" + siteId +
                ", subDomain='" + subDomain + '\''.toString() +
                ", expiresAt=" + expiresAt +
                ", domain='" + domain + '\''.toString() +
                ", countryCode='" + countryCode + '\''.toString() +
                '}'.toString()
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val wmUser = o as WMUser?

        if (if (userId != null) userId != wmUser!!.userId else wmUser!!.userId != null)
            return false
        if (if (token != null) token != wmUser.token else wmUser.token != null)
            return false
        if (if (domain != null) domain != wmUser.domain else wmUser.domain != null)
            return false
        if (if (subDomain != null) subDomain != wmUser.subDomain else wmUser.subDomain != null)
            return false
        if (if (countryCode != null) countryCode != wmUser.countryCode else wmUser.countryCode != null)
            return false
        return if (siteId != null) siteId == wmUser.siteId else wmUser.siteId == null

    }

    override fun hashCode(): Int {
        var result = if (userId != null) userId!!.hashCode() else 0
        result = 31 * result + if (token != null) token!!.hashCode() else 0
        result = 31 * result + if (domain != null) domain!!.hashCode() else 0
        result = 31 * result + if (subDomain != null) subDomain!!.hashCode() else 0
        result = 31 * result + if (countryCode != null) countryCode!!.hashCode() else 0
        result = 31 * result + if (siteId != null) siteId!!.hashCode() else 0
        return result
    }
}
