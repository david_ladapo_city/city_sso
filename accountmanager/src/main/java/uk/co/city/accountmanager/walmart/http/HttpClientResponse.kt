package uk.co.city.accountmanager.walmart.http

/**
 * Created by awhelms on 11/2/15.
 */
class HttpClientResponse {
    var response: String? = null
    var headers: Map<String, List<String>>? = null
}
