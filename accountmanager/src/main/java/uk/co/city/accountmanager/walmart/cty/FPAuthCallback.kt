@file:Suppress("DEPRECATION")

package uk.co.city.accountmanager.walmart.cty

import android.annotation.TargetApi
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import uk.co.city.accountmanager.R
import uk.co.city.accountmanager.walmart.sso.BiometricDialog
import uk.co.city.accountmanager.walmart.sso.WMBiometricSharedPref
import uk.co.city.accountmanager.walmart.sso.WMCryptoKeystoreImpl
import uk.co.city.accountmanager.walmart.sso.WMLoginActivity

@Suppress("DEPRECATION")
class FPAuthCallback(val walmart: WMLoginActivity.WalmartAuthProperties, val activity: AppCompatActivity,
                     private val sharedPref: WMBiometricSharedPref, private val biometricDialog: BiometricDialog): FingerprintManagerCompat.AuthenticationCallback() {


    override fun onAuthenticationError(errMsgId: Int, errString: CharSequence?) {
        super.onAuthenticationError(errMsgId, errString)
        activity.finish()
    }


    @TargetApi(Build.VERSION_CODES.M)
    override fun onAuthenticationSucceeded(result: FingerprintManagerCompat.AuthenticationResult?) {
        super.onAuthenticationSucceeded(result)
        val crypto = WMCryptoKeystoreImpl()
        crypto.setContext(activity)
        sharedPref.setUserBiometricInfo(true, activity)
        sharedPref.storeUserCreds(
            crypto.encrypt(walmart.userid)!!,
            crypto.encrypt(walmart.password)!!,
            walmart.domain!!,
            walmart.siteId,
            walmart.country!!,
            activity
        ) //Encrypts and stores the user's login information
        if (!activity.isFinishing) {
            //show dialog
            biometricDialog.dismissDialog() //dismisses the custom dialog after authenticate
        }
        activity.finish()
    }

    override fun onAuthenticationFailed() {
        super.onAuthenticationFailed()
        biometricDialog.setSubMessage(activity.getString(R.string.fp_not_recognized))
    }
}

