package uk.co.city.accountmanager.walmart.resource

import android.content.Context
import android.content.res.Resources

/**
 * 12/18/15.
 */
class ResourceHelper(ctx: Context) {
    private val resources: Resources
    private val packageName: String

    init {
        packageName = ctx.applicationContext.packageName
        resources = ctx.applicationContext.resources
    }

    fun getString(name: String): String {
        return resources.getString(resources.getIdentifier(name, "string", packageName))
    }
}
