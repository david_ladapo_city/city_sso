package uk.co.city.accountmanager.walmart.sso

import android.content.Context
import uk.co.city.core.walmart.wmsso.WMUser


interface WMAccountStore {
    //fun setContext(context: Context)

    fun getUser(result: GetUserResult)

    fun saveUser(user: WMUser?, result: SaveUserResult)

    interface GetUserResult {
        fun onSuccess(user: WMUser?)

        fun onError(t: Throwable)
    }

    interface SaveUserResult {
        fun onSuccess()

        fun onError(t: Throwable)
    }
}
