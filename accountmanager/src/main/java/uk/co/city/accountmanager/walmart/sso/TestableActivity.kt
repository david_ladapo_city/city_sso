package uk.co.city.accountmanager.walmart.sso

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import uk.co.city.core.walmart.wmsso.WMAccountManagerCallback
import uk.co.city.core.walmart.wmsso.WMAccountManagerServiceClient
import uk.co.city.core.walmart.wmsso.WMUser

/**
 * Created by a0barth on 4/2/18.
 */

class TestableActivity : AppCompatActivity() {
    private var client: WMAccountManagerServiceClient? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        client = WMAccountManagerServiceClient(this)
    }

    override fun onResume() {
        super.onResume()
        getUser()
    }

    private fun getUser() {
        try {
            client!!.getUser(this,
                displayLoginActivity,
                callback
            )
        } catch (e: Exception) {
            Log.e(TAG, "Error while attempting to call getCurrentUser", e)
            Log.d(TAG, "AWH - getting user false; exception")
        }

    }

    companion object {
        private val TAG = "TestableActivity"

        private var displayLoginActivity = true


        fun setDisplayLoginActivity(display: Boolean) {
            displayLoginActivity = display
        }

        private val callback = object : WMAccountManagerCallback {
            override fun onSuccess(user: WMUser?) {
                Log.d(TAG, "getUser() success")
            }

            override fun onError(t: Throwable) {
                Log.e(TAG, "Error while processing callback from getCurrentUser", t)

            }
        }
    }
}