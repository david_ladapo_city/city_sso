package uk.co.city.accountmanager.walmart.sso

import android.accounts.Account
import android.accounts.AccountAuthenticatorActivity
import android.accounts.AccountManager
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Typeface
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.*
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import uk.co.city.accountmanager.walmart.resource.JSONAssetReader
import uk.co.city.core.walmart.wmsso.*
import uk.co.city.core.walmart.component.CryptoUtil
import kotlinx.android.synthetic.main.activity_wm_account_authenticator.*
import uk.co.city.accountmanager.R

import java.util.Date

class WMAccountAuthenticatorActivity : AccountAuthenticatorActivity() {

    private val TAG = "WMAccountAuthActivity"

    private var auth: WMAccountAuthentication? = null

    private var dropdownListData: Map<String, List<Map<String, String>>>? = null
    private var acctMgr: AccountManager? = null
    private var acctHelper: WMAccountHelper? = null

    private var useridField: EditText? = null
    private var passwordField: EditText? = null
    private var countryDropdown: Spinner? = null
    private var domainDropdown: Spinner? = null
    private var subDomainDropdown: Spinner? = null
    private var siteIdField: EditText? = null
    private var progressBarContainer: FrameLayout? = null


    override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        setContentView(R.layout.activity_wm_account_authenticator)

        auth = WMAccountAuthenticationFactory.getInstance(this)

        // set up icon
        val ssoCustomFont = Typeface.createFromAsset(assets, "SSO-custom.ttf")

        val icon = acc_icon
        icon.text = SSOIconCharCode.StrokeCircleSolidKey.code()
        icon.typeface = ssoCustomFont
        icon.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        icon.setTextSize(TypedValue.COMPLEX_UNIT_SP, 256f)

        val signIn = acc_sign_in
        signIn.setOnClickListener { authenticate() }
        acctMgr = AccountManager.get(this.baseContext)
        acctHelper = WMAccountHelper(this.baseContext)

        useridField = acc_userid
        passwordField = acc_password
        domainDropdown = acc_domain
        subDomainDropdown = acc_sub_domain
        countryDropdown = acc_country
        siteIdField = acc_site_id
        progressBarContainer = acc_progressBarContainer

        populateDropdownLists()
        addDropDownListener()
        loadLastUserData()
    }

    private fun addDropDownListener() {
        domainDropdown!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                subDomainDropdown!!.visibility = View.GONE
                siteIdField!!.visibility = View.GONE
                val selectedDomain = (domainDropdown!!.getItemAtPosition(position) as Map<String, String>)["id"]
                if (selectedDomain!!.equals("store", ignoreCase = true)) {
                    siteIdField!!.visibility = View.VISIBLE
                } else if (selectedDomain.equals("dc", ignoreCase = true)) {
                    siteIdField!!.visibility = View.VISIBLE
                    subDomainDropdown!!.visibility = View.VISIBLE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Do nothing
            }
        }
    }

    private fun populateDropdownLists() {
        val assetReader = JSONAssetReader()
        dropdownListData = assetReader.getKeyValuePairResources(this, "dropdown_lists.json")

        populateDropdownList(domainDropdown, dropdownListData!!["domains"])
        populateDropdownList(subDomainDropdown, dropdownListData!!["subDomains"])
        populateDropdownList(countryDropdown, dropdownListData!!["countries"])
    }

    private fun populateDropdownList(dropdownList: Spinner?, nameDesc: List<Map<String, String>>?) {
        if (dropdownList != null && nameDesc != null) {
            val from = arrayOf("desc")
            val to = intArrayOf(android.R.id.text1)

            val simpleAdapter = SimpleAdapter(
                this, nameDesc,
                android.R.layout.simple_spinner_item, from, to
            )
            simpleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            val viewBinder = SimpleAdapter.ViewBinder { view, data, textRepresentation ->
                val textView = view as TextView
                textView.text = textRepresentation
                true
            }
            simpleAdapter.viewBinder = viewBinder
            dropdownList.adapter = simpleAdapter
        }
    }

    private fun loadLastUserData() {
        val lastUserAccount = acctHelper!!.mostRecentUser
        if (lastUserAccount != null) {
            val lastUser = WMUser()
            WMUserAdapter.fromAccount(acctMgr!!, lastUserAccount, lastUser)

            useridField!!.setText(lastUser.userId)

            lastUser.domain?.let {
                setDropdownValue(domainDropdown, "domains", it)
            }

            lastUser.subDomain?.let {
                setDropdownValue(subDomainDropdown, "subDomains", it)
            }
            lastUser.siteId?.let {
                siteIdField!!.setText(it.toString())
            }

            lastUser.countryCode?.let {
                setDropdownValue(countryDropdown, "countries", it)
            }
        }
    }

    private fun setDropdownValue(dropdownList: Spinner?, listDataKey: String, idValue: String) {
        var i = 0

        i = 0
        while (i < dropdownListData!![listDataKey]!!.size) {
            if (dropdownListData!![listDataKey]!![i]["id"]!!.equals(idValue, ignoreCase = true)) {
                break
            }
            i++
        }
        dropdownList!!.setSelection(i)
    }

    override fun onResume() {
        super.onResume()

        val acct = acctHelper!!.currentWMAccount
        acct?.let {
            Log.d(TAG, "About to send broadcast with action " + SSOIntentType.UserSignedIn.action)
            val i = Intent(SSOIntentType.UserSignedIn.action).apply {
                putExtras(WMUserAdapter.toBundle(WMUserAdapter.fromAccount(acctMgr!!, it)))
            }
            LocalBroadcastManager.getInstance(this).sendBroadcast(i)
            this.finish()
        }
    }

    fun inputsAreValid(): Boolean {
        var msg =
            resources!!.getString(resources!!.getIdentifier("pleaseFillInFields", "string", packageName)) + ":\n\n"
        var valid: Boolean? = true

        val pair2 = validateUserId(msg, valid)
        msg = pair2.first
        valid = pair2.second

        val pair1 = validatePassword(msg, valid)
        msg = pair1.first
        valid = pair1.second

        val domain = getDomain()
        val subDomain = getSubDomain()
        val country = getCountry()
        val siteId = siteIdField!!.text.toString()

        val pair = validateDomain(domain, msg, valid, siteId, subDomain)
        msg = pair.first
        valid = pair.second

        if (!valid!!) {
            showAlert(resources!!.getString(resources!!.getIdentifier("missingFields", "string", packageName)), msg)
        }

        return valid!!
    }

    private fun validateUserId(msg: String, valid: Boolean?): Pair<String, Boolean?> {
        var msg1 = msg
        var valid1 = valid
        val userid = useridField!!.text.toString()
        if (userid.trim { it <= ' ' }.length == 0) {
            msg1 += "- " + resources!!.getString(resources!!.getIdentifier("userid", "string", packageName)) + "\n"
            valid1 = false
        }
        return Pair(msg1, valid1)
    }

    private fun validatePassword(msg: String, valid: Boolean?): Pair<String, Boolean?> {
        var msg1 = msg
        var valid1 = valid
        val password = passwordField!!.text.toString()
        if (password.trim { it <= ' ' }.isEmpty()) {
            msg1 += "- " + resources!!.getString(resources!!.getIdentifier("password", "string", packageName)) + "\n"
            valid1 = false
        }
        return Pair(msg1, valid1)
    }

    private fun validateDomain(
        domain: String?,
        msg: String,
        valid: Boolean?,
        siteId: String,
        subDomain: String?
    ): Pair<String, Boolean?> {
        var msg1 = msg
        var valid1 = valid
        if (domain!!.isEmpty()) {
            msg1 += "- " + resources!!.getString(R.string.domain) + "\n"
            valid1 = false
        } else if (domain.equals(SSOConstants.STORE_DOMAIN, ignoreCase = true)) {
            if (siteId.trim { it <= ' ' }.isEmpty()) {
                msg1 += "- " + resources!!.getString(R.string.site) + "\n"
                valid1 = false
            }
        } else if (domain.equals(SSOConstants.DC_DOMAIN, ignoreCase = true)) {
            if (siteId.trim { it <= ' ' }.isEmpty()) {
                msg1 += "- " + resources!!.getString(resources!!.getIdentifier("site", "string", packageName)) + "\n"
                valid1 = false
            }
            if (subDomain!!.isEmpty()) {
                msg1 += "- " + resources!!.getString(
                    resources!!.getIdentifier(
                        "password",
                        "string",
                        packageName
                    )
                ) + "\n"
                valid1 = false
            }
        }
        return Pair(msg1, valid1)
    }

    private fun getCountry(): String? {
        return if (countryDropdown!!.selectedItem != null)
            (countryDropdown!!.selectedItem as Map<String, String>)["id"]
        else
            ""
    }

    private fun getSubDomain(): String? {
        return if (subDomainDropdown!!.selectedItem != null)
            (subDomainDropdown!!.selectedItem as Map<String, String>)["id"]
        else
            ""
    }

    private fun getDomain(): String? {
        return if (domainDropdown!!.selectedItem != null)
            (domainDropdown!!.selectedItem as Map<String, String>)["id"]
        else
            ""
    }

    fun authenticate() {
        if (inputsAreValid()) {
            val userid = useridField!!.text.toString()
            val password = passwordField!!.text.toString()
            val domain = getDomain()
            val subDomain = getSubDomain()
            val country = getCountry()
            val siteId = siteIdField!!.text.toString()

            progressBarContainer!!.visibility = View.VISIBLE

            object : AsyncTask<Void, Void, Intent>() {

                override fun doInBackground(vararg params: Void): Intent {
                    val intent = Intent()

                    val user = WMUser()
                    user.userId = userid
                    user.domain = domain
                    user.subDomain = subDomain
                    user.countryCode = country
                    if (siteId != null && siteId.trim { it <= ' ' } != "") {
                        user.siteId = java.lang.Long.parseLong(siteId)
                    } else {
                        user.siteId = null
                    }
                    auth!!.authenticate(user, password)

                    Log.d(TAG, "AWH ---- authenticated user; token exipres at " + user.expiresAt!!)

                    intent.putExtras(WMUserAdapter.toBundle(user))

                    return intent
                }

                override fun onPostExecute(intent: Intent) {
                    finishLogin(intent)
                }
            }.execute()

        }
    }

    private fun finishLogin(intent: Intent) {
        val userid = intent.getStringExtra(WMUserAdapter.USERID)
        var authToken: String? = intent.getStringExtra(WMUserAdapter.TOKEN)
        val accountType = SSOConstants.WM_ACCT_TYPE

        if (authToken != null && authToken.trim { it <= ' ' } != "") {
            var matching = acctHelper!!.getWMAccountByUserId(userid)
            acctMgr?.let {
                if (matching == null) {
                    matching = Account(userid, accountType)
                    it.addAccountExplicitly(matching, null, null)
                }

                // TODO - encrypt token
                try {
                    authToken = CryptoUtil().encrypt(authToken.toString())
                } catch (e: Exception) {
                    Log.e(TAG, "Error encrypting auth token", e)
                }

                Log.d(TAG, "finished login; setting token in acctMgr [$authToken]")

                addAccountData(it, matching, authToken, intent)


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    it.notifyAccountAuthenticated(matching)
                }


            }

            this.setAccountAuthenticatorResult(intent.extras)
            this.setResult(Activity.RESULT_OK, intent)
            this.finish()
        } else {
            progressBarContainer!!.visibility = View.GONE

            var msg: String? = intent.getStringExtra(WMUserAdapter.AUTH_RESULT_MSG)
            if (msg == null || msg.trim { it <= ' ' } == "") {
                msg = resources!!.getString(resources!!.getIdentifier("errorWhileSigningIn", "string", packageName))
            }
            showAlert(resources!!.getString(resources!!.getIdentifier("errorTitle", "string", packageName)), msg!!)
        }
    }

    private fun addAccountData(
        accountManager: AccountManager,
        matching: Account?,
        authToken: String?,
        intent: Intent
    ) {

        accountManager.apply {
            setAuthToken(matching, SSOConstants.WM_AUTH_TOKEN_TYPE, authToken)

            setUserData(matching, WMUserAdapter.COUNTRY, intent.getStringExtra(WMUserAdapter.COUNTRY))
            setUserData(
                matching,
                WMUserAdapter.SITE,
                intent.getLongExtra(WMUserAdapter.SITE, 0).toString()
            )
            setUserData(matching, WMUserAdapter.DOMAIN, intent.getStringExtra(WMUserAdapter.DOMAIN))
            setUserData(
                matching,
                WMUserAdapter.SUB_DOMAIN,
                intent.getStringExtra(WMUserAdapter.SUB_DOMAIN)
            )
            setUserData(
                matching,
                WMUserAdapter.REFRESH_TOKEN,
                intent.getStringExtra(WMUserAdapter.REFRESH_TOKEN)
            )
            setUserData(
                matching,
                WMUserAdapter.VALIDITY,
                intent.getLongExtra(WMUserAdapter.VALIDITY, 0).toString()
            )
            setUserData(
                matching,
                WMUserAdapter.ADDITIONAL,
                intent.getStringExtra(WMUserAdapter.ADDITIONAL)
            )
        }
        Log.d(
            TAG,
            "finishLogin() - expiration at " + intent.getLongExtra(WMUserAdapter.EXPIRES_AT, 0).toString()
        )
        accountManager.apply {

            setUserData(
                matching,
                WMUserAdapter.EXPIRES_AT,
                intent.getLongExtra(WMUserAdapter.EXPIRES_AT, 0).toString()
            )
            setUserData(matching, SSOConstants.LAST_LOGIN_TIME, String.format("%d", Date().time))
        }

    }

    private fun showAlert(title: String, msg: String) {
        val builder = AlertDialog.Builder(this).apply {
            setMessage(msg).setTitle(title)
            setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss() }
        }

        val dialog = builder.create()
        dialog.show()
    }

    private enum class SSOIconCharCode private constructor(private val `val`: String) {
        StrokeCircleSolidKey("\ue905"),
        SolidCircleKey("\ue900"),
        StrokeCircleKey("\ue901"),
        User("\ue902"),
        Key("\ue903"),
        Lock("\ue904"),
        Forward("\ue607");

        fun code(): String {
            return `val`
        }
    }
}
