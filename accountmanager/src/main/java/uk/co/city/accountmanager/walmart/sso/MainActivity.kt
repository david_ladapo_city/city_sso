package uk.co.city.accountmanager.walmart.sso

import android.app.AlertDialog
import android.content.*
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager

import uk.co.city.accountmanager.R
import uk.co.city.core.walmart.wmsso.*

class MainActivity : AppCompatActivity() {

    private var userId: TextView? = null
    private var domain: TextView? = null
    private var subDomain: TextView? = null
    private var site: TextView? = null
    private var country: TextView? = null
    private var token: TextView? = null


    private val user: WMUser? = null
    private var client: WMAccountManagerServiceClient? = null

    private var ssoReceiver: BroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userId = findViewById<View>(R.id.userId) as TextView
        domain = findViewById<View>(R.id.domain) as TextView
        subDomain = findViewById<View>(R.id.subDomain) as TextView
        site = findViewById<View>(R.id.site) as TextView
        country = findViewById<View>(R.id.country) as TextView
        token = findViewById<View>(R.id.token) as TextView

        val signOutButton = findViewById<View>(R.id.signOutButton) as Button
        signOutButton.setOnClickListener { signOutUser() }

        client = WMAccountManagerServiceClient(this)

        ssoReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val tp = SSOIntentType.getByAction(intent.action)

                val user = WMUserAdapter.fromBundle(intent.extras!!)

                when (tp) {
                    SSOIntentType.UserSignedIn -> Log.d(TAG, "Received user signed in for user " + user!!)
                    SSOIntentType.UserSignedOut -> Log.d(TAG, "Received user signed out for user " + user!!)
                    SSOIntentType.UserChanged -> {
                        Log.d(TAG, "Received user changed for user " + user!!)
                        val changedFields = intent.getStringArrayListExtra(SSOConstants.EXTRA_CHANGED_FIELDS)
                        if (changedFields != null) {
                            for (field in changedFields) {

                                Log.d(TAG, "changed field [" + field + "] " + intent.extras!!.get(field))
                            }
                        }
                    }
                }
            }
        }

        val filter = IntentFilter()
        filter.addAction(SSOIntentType.UserChanged.action)
        filter.addAction(SSOIntentType.UserSignedIn.action)
        filter.addAction(SSOIntentType.UserSignedOut.action)

        LocalBroadcastManager.getInstance(this).registerReceiver(ssoReceiver!!, filter)


    }

    override fun onDestroy() {
        super.onDestroy()

        if (ssoReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(ssoReceiver!!)
            ssoReceiver = null
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)

    }

    override fun onResume() {
        super.onResume()

        Log.d(TAG, "AWH - onResume")
        //        getUser();
    }

    private fun getUser() {
        try {
            client!!.getUser(this, object : WMAccountManagerCallback {
                override fun onSuccess(user: WMUser?) {
                    Log.d(TAG, "getUser() success")
                    user?.let {wmuser->

                        userId!!.text = wmuser.userId
                        domain!!.text = wmuser.domain

                        if (wmuser.subDomain != null && wmuser.subDomain!!.length > 0) {
                            subDomain!!.text = wmuser.subDomain
                            subDomain!!.visibility = View.VISIBLE
                        } else {
                            subDomain!!.visibility = View.GONE
                        }

                        if (wmuser.siteId != null && wmuser.siteId!! > 0) {
                            site!!.text = wmuser.siteId!!.toString()
                            site!!.visibility = View.VISIBLE
                        } else {
                            site!!.visibility = View.GONE
                        }

                        if (wmuser.countryCode != null && wmuser.countryCode!!.length > 0) {
                            country!!.text = wmuser.countryCode
                            country!!.visibility = View.VISIBLE
                        } else {
                            country!!.visibility = View.GONE
                        }

                        token!!.text = wmuser.token

                        Log.d(TAG, "User as JSON is < " + WMUserAdapter.toJSON(wmuser).toString() + " >")

                    }

                }

                override fun onError(t: Throwable) {
                    showAlert(getString(R.string.errorTitle), getString(R.string.errorWhileSigningIn))
                    Log.e(TAG, "Error while processing callback from getCurrentUser", t)

                }
            })

        } catch (e: Exception) {
            showAlert(getString(R.string.errorTitle), getString(R.string.errorWhileSigningIn))
            Log.e(TAG, "Error while attempting to call getCurrentUser", e)
            Log.d(TAG, "AWH - getting user false; exception")
        }

    }

    private fun signOutUser() {
        client!!.signOutUser(object : WMAccountManagerCallback {
            override fun onSuccess(user: WMUser?) {
                Log.d(TAG, "signed out")
                val h = Handler()
                h.post { getUser() }
            }

            override fun onError(t: Throwable) {
                Log.e(TAG, "error trying to sign out", t)
            }
        })
    }

    private fun showAlert(title: String, msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(msg).setTitle(title)
        builder.setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss() }

        val dialog = builder.create()
        dialog.show()
    }

    companion object {

        private val TAG = "MainActivity"
    }
}
