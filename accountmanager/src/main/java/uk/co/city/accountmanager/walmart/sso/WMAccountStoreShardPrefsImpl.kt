package uk.co.city.accountmanager.walmart.sso

import android.content.Context
import android.content.SharedPreferences
import uk.co.city.core.walmart.wmsso.WMUser
import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main
import org.json.JSONException
import org.json.JSONObject

/**
 *
 */
class WMAccountStoreShardPrefsImpl(val context: Context) : WMAccountStore {

    private class SaveUserParams(var user: WMUser, var result: WMAccountStore.SaveUserResult)


    override fun getUser(result: WMAccountStore.GetUserResult) {
        //context?.let {
            getUserAsync(result)
        //} ?: result.onError(Error("context not set on account store"))

    }

    override fun saveUser(user: WMUser?, result: WMAccountStore.SaveUserResult) {
        //context?.let {
            user?.let { localUser ->
                saveUserAsync(SaveUserParams(localUser, result))
            } ?: result.onError(Error("user object to save is null"))

       // } ?: result.onError(Error("context not set on account store"))
    }

    private fun clean(editor: SharedPreferences.Editor) {
        for (key in prefsKeys) {
            editor.remove(key)
        }
    }


    fun getUserAsync(vararg params: WMAccountStore.GetUserResult): WMUser? {
        var user: WMUser? = null
        lateinit var result: WMAccountStore.GetUserResult
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.Default) {
                result = params[0]
                val preferences = context!!.getSharedPreferences(PREFS, 0)
                var userJsonStr = preferences.getString(KEY_PREFS_WM_USER, null)
                var user: WMUser? = null
                userJsonStr?.let {
                    val crypto = WMCryptoKeystoreImpl()
                    crypto.setContext(context!!)
                    userJsonStr = crypto.decrypt(userJsonStr)

                    try {
                        user = getUserFromJson(userJsonStr)
                    } catch (e: JSONException) {
                        result.onError(Error("error parsing userJson string $userJsonStr as json", e))
                    }

                }
            }
            result.onSuccess(user)
        }
        return user
    }

    private fun getUserFromJson(userJsonStr: String?): WMUser {
        val userJson = JSONObject(userJsonStr)
        val user = WMUser()
        user.userId = userJson.getString(KEY_PREFS_USERID)
        user.token = userJson.optString(KEY_PREFS_TOKEN)
        user.domain = userJson.getString(KEY_PREFS_DOMAIN)
        user.subDomain = userJson.optString(KEY_PREFS_SUBDOMAIN)
        user.countryCode = userJson.getString(KEY_PREFS_COUNTRY)
        if (userJson.has(KEY_PREFS_SITE_NBR)) {
            user.siteId = userJson.getLong(KEY_PREFS_SITE_NBR)
        }
        if (userJson.has(KEY_PREFS_EXPIRES)) {
            user.expiresAt = userJson.getLong(KEY_PREFS_EXPIRES)
        }
        user.additional = userJson.optJSONObject(KEY_PREFS_ADDITIONAL)
        return user
    }

    private fun saveUserAsync(vararg params: SaveUserParams) {
        lateinit var result: WMAccountStore.SaveUserResult
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.Default) {
                result = params[0].result
                val user = params[0].user
                val preferences = context!!.getSharedPreferences(PREFS, 0)
                val editor = preferences.edit()
                clean(editor)
                encryptAndSave(user, editor, result)
            }
            result.onSuccess()

        }

    }

    private fun encryptAndSave(
        user: WMUser,
        editor: SharedPreferences.Editor,
        result: WMAccountStore.SaveUserResult
    ): Any {
        return try {
            var userJsonStr = getJsonFromUser(user)

            val crypto = WMCryptoKeystoreImpl()
            crypto.setContext(context!!)
            userJsonStr = crypto.encrypt(userJsonStr)!!
            editor.putString(KEY_PREFS_WM_USER, userJsonStr)
            editor.commit()
        } catch (e: JSONException) {
            result.onError(Error("error converting wmuser to json", e))
        }
    }

    private fun getJsonFromUser(user: WMUser): String {
        val userJson = JSONObject().apply {

            put(KEY_PREFS_USERID, user.userId)
            put(KEY_PREFS_TOKEN, user.token)
            put(KEY_PREFS_DOMAIN, user.domain)
            put(KEY_PREFS_COUNTRY, user.countryCode)
            put(KEY_PREFS_SUBDOMAIN, user.subDomain)
            user.siteId?.let {
                if (it > 0) {
                    put(KEY_PREFS_SITE_NBR, it)
                }
            }
            put(KEY_PREFS_EXPIRES, user.expiresAt)
            put(KEY_PREFS_ADDITIONAL, user.additional)

        }

        return userJson.toString()
    }

    companion object {

        private val PREFS = "WMAccount"
        private val KEY_PREFS_USERID = "userId"
        private val KEY_PREFS_TOKEN = "token"
        private val KEY_PREFS_DOMAIN = "domain"
        private val KEY_PREFS_SITE_NBR = "siteNbr"
        private val KEY_PREFS_COUNTRY = "country"
        private val KEY_PREFS_SUBDOMAIN = "subDomain"
        private val KEY_PREFS_ADDITIONAL = "additional"

        private val KEY_PREFS_EXPIRES = "expires"
        //    private static final String KEY_PREFS_ENV = "env";
        private val KEY_PREFS_WM_USER = "wmUser"

        private val prefsKeys =
            arrayOf(/*KEY_PREFS_USERID, KEY_PREFS_TOKEN, KEY_PREFS_DOMAIN, KEY_PREFS_SITE_NBR, KEY_PREFS_COUNTRY, KEY_PREFS_SUBDOMAIN, KEY_PREFS_ADDITIONAL, KEY_PREFS_EXPIRES,*/
                KEY_PREFS_WM_USER
            )
    }
}
