package uk.co.city.accountmanager.walmart.resource

import android.app.Activity
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber

import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset
import java.util.*

/**
 */
class JSONAssetReader : AssetReader {

    override fun getKeyValuePairResources(
        activity: Activity,
        filename: String
    ): MutableMap<String, List<Map<String, String>>> {

        val json = loadJSONFromAsset(activity, filename)

        return parse(json)
    }

    private fun loadJSONFromAsset(activity: Activity, filename: String): String? {
        var json: String? = null


        try {
            var ins: InputStream = activity.assets.open(filename)
            json = ins.bufferedReader(Charset.defaultCharset()).use { it.readText() }

        } catch (e: IOException) {
            Timber.e("Error while reading asset file $filename", e)
        }

        return json
    }

    private fun parse(jsonStr: String?): MutableMap<String, List<Map<String, String>>> {
        val returnMap = HashMap<String, List<Map<String, String>>>()
        Timber.d("About to parse the json string: ${jsonStr}")
        if (jsonStr != null) {
            try {
                val root = JSONObject(jsonStr)

                val listTypes = root.keys()

                while (listTypes.hasNext()) {
                    val listType = listTypes.next()
                    val list = root.getJSONArray(listType)
                    val returnList = ArrayList<HashMap<String, String>>()

                    for (i in 0 until list.length()) {
                        val listItem = list.getJSONObject(i)

                        val itemToAdd = HashMap<String, String>()
                        itemToAdd["id"] = listItem.getString("id")
                        itemToAdd["desc"] = listItem.getString("desc")
                        returnList.add(itemToAdd)
                    }
                    returnMap[listType] = returnList
                }
            } catch (e: JSONException) {
                Timber.e("Error while parsing asset string <$jsonStr>", e)
            }

        }
        Timber.d("Returning complex ob ject: ${returnMap}")
        return returnMap
    }

}
