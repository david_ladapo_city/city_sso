package uk.co.city.accountmanager.walmart.sso

import android.accounts.*
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import uk.co.city.core.walmart.wmsso.SSOConstants
import uk.co.city.core.walmart.wmsso.WMUser
import uk.co.city.core.walmart.wmsso.WMUserAdapter
import uk.co.city.core.walmart.component.CryptoUtil
import timber.log.Timber

/**
 */
class WMAccountAuthenticator(private val ctx: Context) : AbstractAccountAuthenticator(ctx) {
    private val authentication: WMAccountAuthentication =
        WMAccountAuthenticationFactory.getInstance(ctx)


    @Throws(NetworkErrorException::class)
    override fun addAccount(
        response: AccountAuthenticatorResponse, accountType: String, authTokenType: String,
        requiredFeatures: Array<String>, options: Bundle
    ): Bundle {
        val intent: Intent = Intent(ctx, WMAccountAuthenticatorActivity::class.java).apply {
            putExtra(SSOConstants.ACCT_TYPE, accountType)
            putExtra(SSOConstants.AUTH_TYPE, authTokenType)
            putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
        }
        val result = Bundle().apply {
            putParcelable(AccountManager.KEY_INTENT, intent)
        }
        return result
    }

    @Throws(NetworkErrorException::class)
    override fun getAuthToken(
        response: AccountAuthenticatorResponse, account: Account, authTokenType: String,
        options: Bundle
    ): Bundle {

        val am = AccountManager.get(ctx)
        val result = Bundle()

        Timber.d( "called getAuthToken")

        var authToken = am.peekAuthToken(account, authTokenType)

        if (TextUtils.isEmpty(authToken)) {
            setupResultBundleForPrompt(result, response, account, authTokenType)
        } else {
            // decrypt token
            try {
                authToken = CryptoUtil().decrypt(authToken)
            } catch (e: Exception) {
                Timber.e( "Couldn't decrypt authToken: ${e.message}")
                authToken = ""
            }

            val user = WMUser()
            WMUserAdapter.fromAccount(am, account, user)
            user.token = authToken

            if (authentication.isTokenValid(user)) {
                result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name)
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type)
                result.putString(AccountManager.KEY_AUTHTOKEN, authToken)
            } else {
                setupResultBundleForPrompt(result, response, account, authTokenType)
            }
        }

        return result
    }

    private fun setupResultBundleForPrompt(
        result: Bundle, response: AccountAuthenticatorResponse,
        account: Account, authTokenType: String
    ) {
        val intent = Intent(ctx, WMAccountAuthenticatorActivity::class.java).apply {
            putExtra(SSOConstants.ACCT_NAME, account.name)
            putExtra(SSOConstants.AUTH_TYPE, authTokenType)
            putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
        }
        result.putParcelable(AccountManager.KEY_INTENT, intent)
    }

    override fun getAuthTokenLabel(authTokenType: String): String? {
        return null
    }

    @Throws(NetworkErrorException::class)
    override fun hasFeatures(
        response: AccountAuthenticatorResponse, account: Account,
        features: Array<String>
    ): Bundle {
        val result = Bundle()
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false)
        return result
    }

    @Throws(NetworkErrorException::class)
    override fun confirmCredentials(
        response: AccountAuthenticatorResponse, account: Account,
        options: Bundle
    ): Bundle? {
        return null
    }

    override fun editProperties(response: AccountAuthenticatorResponse, accountType: String): Bundle? {
        return null
    }

    @Throws(NetworkErrorException::class)
    override fun updateCredentials(
        response: AccountAuthenticatorResponse, account: Account, authTokenType: String,
        options: Bundle
    ): Bundle? {
        return null
    }

}
