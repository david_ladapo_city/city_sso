package uk.co.city.accountmanager.walmart.sso

import android.content.Context

/**
 */
interface WMCrypto {
    fun setContext(context: Context)
    fun encrypt(toEncrypt: String?): String?
    fun decrypt(decrypted: String?): String?
}
