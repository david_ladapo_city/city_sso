package uk.co.city.accountmanager.walmart.sso

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.core.os.CancellationSignal
import kotlinx.android.synthetic.main.biometric_dialog.view.*
import uk.co.city.accountmanager.R

class BiometricDialog(
    inflater: LayoutInflater,
    activity: Activity,
    internal var cancellationSignal: CancellationSignal
) {

    internal var bioAuth: AlertDialog.Builder

    internal lateinit var dialog: AlertDialog

    private var finished = false

    private var subText: TextView? = null

    internal lateinit var vi: View

    init {

        bioAuth = AlertDialog.Builder(activity)
        setView(inflater, activity)
    }

    private fun setView(layoutInflater: LayoutInflater, activity: Activity) {
        bioAuth.setView(layoutInflater.inflate(R.layout.biometric_dialog, null))
        vi = layoutInflater.inflate(R.layout.biometric_dialog, null)

        subText = vi.authText

        bioAuth.setPositiveButton("Cancel") { dialog, which ->
            dismissDialog()
            if (finished)
                activity.finish()
        }

        dialog = bioAuth.create()

        dialog.setCanceledOnTouchOutside(false)

    }

    fun showDialog() {
        dialog.show()
    }

    fun cancelAuth() {
        cancellationSignal.cancel()
    }

    fun setFinished(finished: Boolean) {
        this.finished = finished
    }

    fun dismissDialog() {
        dialog.dismiss()
        cancelAuth()
    }

    fun setSubMessage(msg: String) {
        subText!!.text = msg
    }


}
