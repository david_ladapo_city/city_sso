package uk.co.city.accountmanager.walmart.sso

import android.app.Service
import android.content.Intent
import android.os.*
import io.fabric.sdk.android.services.concurrency.AsyncTask.init
import uk.co.city.core.walmart.wmsso.SSOConstants
import uk.co.city.core.walmart.wmsso.SSOIntentType
import uk.co.city.core.walmart.wmsso.WMUser
import uk.co.city.core.walmart.wmsso.WMUserAdapter
import kotlinx.coroutines.*
import timber.log.Timber

/**
9/22/16.
 */
class WMAccountManagerService : Service() {

    private val messenger = Messenger(WMAccountManagerHandler())
    private var accountStore:WMAccountStore = WMAccountStoreShardPrefsImpl(applicationContext)

    override fun onBind(intent: Intent): IBinder? {
        return messenger.binder
    }

    private inner class WMAccountManagerHandler : Handler() {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            val request = SSOConstants.Requests.byVal(msg.what)

            if (request == SSOConstants.Requests.GetUser) {
                getUser(msg)
            } else if (request == SSOConstants.Requests.SignOutUser) {
                signOutUser(msg)
            }
        }
    }

    private fun getUser(msg: Message) {

        val authentication = WMAccountAuthenticationFactory.getInstance(this)
        val replyTo = msg.replyTo
        val bundle = msg.data
        authentication.setAppEnv(bundle.getString("ENV"))
        authentication.setPackageId(bundle.getString("package"))

        val packageName = bundle.getString("package")

        authentication.biometricPref =
            !(bundle.getString("isBiometricAllowed") != null &&
                    bundle.getString("isBiometricAllowed")!!.toLowerCase() == "false")
        accountStore.getUser(getUserResult(authentication, replyTo, bundle))
    }

    inner class getUserResult(
        private val authentication: WMAccountAuthentication,
        private val replyTo: Messenger?,
        private val bundle: Bundle
    ) : WMAccountStore.GetUserResult {
        override fun onSuccess(user: WMUser?) {
            if (user != null && authentication.isTokenValid(user)) {
                respondWithUser(replyTo, user)
            } else {
                // Prompt for login; best done from the calling app who will have an activity to launch the new one from
                replyTo?.let {
                    processReplyTo(replyTo)
                }

            }
        }

        private fun processReplyTo(messenger1: Messenger): Any {
            val displayLoginPresent = bundle.containsKey(SSOConstants.KEY_DISPLAY_LOGIN)
            val displayLogin = bundle.getBoolean(SSOConstants.KEY_DISPLAY_LOGIN)
            return if (displayLoginPresent && !displayLogin) {
                respondWithUser(messenger1, null)
            } else {
                val response = Message.obtain()
                val b = Bundle()
                b.putBoolean(SSOConstants.KEY_LOGIN_REQUIRED, true)
                response.data = b
                try {
                    messenger1.send(response)
                } catch (e: RemoteException) {
                    Timber.e("Caught RemoteException: ${e.message} ")
                }

            }
        }

        override fun onError(t: Throwable) {
            Timber.e("Error getting user: ${t.message} ")
            respondWithUser(replyTo, null)
        }
    }

    private fun signOutUser(msg: Message) {
        val authentication = WMAccountAuthenticationFactory.getInstance(this)
        val replyTo = msg.replyTo
        accountStore.getUser(getSignOutUser(authentication, replyTo, msg))

    }

    inner class getSignOutUser(
        private val authentication: WMAccountAuthentication,
        private val replyTo: Messenger?,
        private val msg: Message
    ) : WMAccountStore.GetUserResult {
        override fun onSuccess(user: WMUser?) {
            user?.let { localUser ->

                GlobalScope.launch(Dispatchers.Default) {
                    withContext(Dispatchers.IO) {
                        authentication.invalidate(localUser)
                        localUser.token = null
                        localUser.expiresAt = null
                        accountStore.saveUser(localUser, object : WMAccountStore.SaveUserResult {
                            override fun onSuccess() {
                                respond(replyTo)
                                localUser?.let {
                                    val i = Intent(SSOIntentType.UserSignedOut.action)
                                    i.putExtras(WMUserAdapter.toBundle(it))
                                    sendBroadcast(i, SSOConstants.PERM_RECV_USER_CHANGED)
                                }
                            }

                            override fun onError(t: Throwable) {
                                Timber.e("Error saving user at sign out: ${t.message}")
                                respond(msg.replyTo)
                            }
                        })
                    }

                }

            }
        }

        override fun onError(t: Throwable) {
            Timber.e("Error getting user b4 sign out: ${t.message}")
            respond(msg.replyTo)
        }
    }

    companion object {

        fun respondWithUser(replyTo: Messenger?, user: WMUser?) {
            if (replyTo != null) {
                val response = Message.obtain()
                response.data = WMUserAdapter.toBundle(user)
                try {
                    replyTo.send(response)
                } catch (e: RemoteException) {
                    Timber.e("RemoteException responding to getUser: ${e.message}")
                }

            }
        }

        fun respond(replyTo: Messenger?) {
            if (replyTo != null) {
                val response = Message.obtain()
                try {
                    replyTo.send(response)
                } catch (e: RemoteException) {
                    Timber.e("RemoteException responding to signOutUser: ${e.message}")
                }

            }

        }
    }
}
