package uk.co.city.accountmanager.walmart.http

import android.util.Log
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.net.UnknownHostException
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.*

/**
 * Created by awhelms on 10/9/15.
 */
class HttpClient {
    private val TAG = "HttpClient"

    private inner class NullHostNameVerifier : HostnameVerifier {
        override fun verify(hostname: String, session: SSLSession): Boolean {
            Log.d(TAG, "inside nullhostnameverifier verify")
            return true
        }
    }

    private fun disableSSLCertificateChecking() {
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                Log.d(TAG, "inside checkClientTrusted")
            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                Log.d(TAG, "inside checkServerTrusted")

            }

            override fun getAcceptedIssuers(): Array<X509Certificate>? {
                Log.d(TAG, "inside getAcceptedIssuers")

                return null
            }
        })

        try {
            enableTLSv12(trustAllCerts)
            HttpsURLConnection.setDefaultHostnameVerifier(NullHostNameVerifier())
        } catch (e: Exception) {

        }

    }

    private fun enableTLSv12(trustManagers: Array<TrustManager>? = null) {
        try {
            HttpsURLConnection.setDefaultSSLSocketFactory(
                TLSSocketFactory(
                    null!!,
                    trustManagers!!,
                    SecureRandom()
                )
            )
        } catch (e: Exception) {

        }

    }

    @Throws(HttpClientException::class)
    @JvmOverloads
    operator fun get(uri: String, headers: Map<String, String>, timeout: Int = 10000): HttpClientResponse {
        return processRequest(uri, "GET", headers, null, timeout)
    }

    @Throws(HttpClientException::class)
    @JvmOverloads
    fun post(uri: String, headers: Map<String, String>, postData: String, timeout: Int = 10000): HttpClientResponse {
        return processRequest(uri, "POST", headers, postData, timeout)
    }

    @Throws(HttpClientException::class)
    @JvmOverloads
    fun delete(uri: String, headers: Map<String, String>, postData: String, timeout: Int = 10000): HttpClientResponse {
        return processRequest(uri, "DELETE", headers, postData, timeout)
    }

    @Throws(HttpClientException::class)
    private fun processRequest(
        uri: String, method: String,
        headers: Map<String, String>?, postData: String?,
        timeout: Int
    ): HttpClientResponse {
        val clientResponse = HttpClientResponse()
        val url: URL
        val urlConnection: HttpURLConnection

        val requestBytes: ByteArray

        enableTLSv12()

        try {

            Log.d(TAG, "Opening connection to $uri")
            url = URL(uri)

            HttpURLConnection.setFollowRedirects(false)
            urlConnection = url.openConnection() as HttpURLConnection
            urlConnection.requestMethod = method
            urlConnection.connectTimeout = timeout
            urlConnection.readTimeout = timeout
        } catch (e: Exception) {
            Log.e(TAG, "Error while setting up connection", e)
            throw HttpClientException("Error while setting up connection", e)
        }

        if (headers != null) {
            for (key in headers.keys) {
                urlConnection.setRequestProperty(key, headers[key])
            }
        }

        if (postData != null) {
            try {
                requestBytes = postData.toByteArray(charset("UTF-8"))
                urlConnection.doOutput = true
                urlConnection.setFixedLengthStreamingMode(requestBytes.size)

            } catch (e: Exception) {
                Log.e(TAG, "Error while parsing requestData", e)
                throw HttpClientException("Error while parsing requestData", e)
            }

            val out: OutputStream
            try {

                out = BufferedOutputStream(urlConnection.outputStream)
                out.write(requestBytes)
                out.close()
            } catch (e: Exception) {
                Log.e(TAG, "Error while writing out to connection", e)
                urlConnection.disconnect()
                throw HttpClientException(
                    "Error while writing out to connection",
                    e
                )
            }

        }


        var response = ""
        var responseCode = 0
        var responseMessage = ""
        var inputStream: InputStream? = null

        try {
            responseMessage = urlConnection.responseMessage
            Log.d(TAG, "Response object: $responseMessage")
            responseCode = urlConnection.responseCode
        } catch (se: SSLHandshakeException) {
            Log.e(TAG, "SSL Handshake exception", se)
            if (se.cause != null) {
                Log.e(TAG, "further info", se.cause)

                if (se.cause?.cause != null) {
                    Log.e(TAG, "even further info", se.cause?.cause)

                    if (se.cause?.cause?.cause != null) {
                        Log.e(TAG, "yet more info", se.cause?.cause?.cause)
                    }
                }
            }
            throw HttpClientException(se)
        } catch (e: UnknownHostException) {
            Log.e(TAG, "Unknown host $url", e)
            throw HttpClientException("Unknown host $url", e)
        } catch (e: Exception) {
            Log.e(TAG, "Error while reading response code", e)
        }

        if (responseCode != HttpsURLConnection.HTTP_OK) {
            Log.e(TAG, "Bad response code from connection $responseCode:$responseMessage")
        }


        try {
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                Log.d(TAG, "using input stream")
                inputStream = BufferedInputStream(urlConnection.inputStream)
            } else {
                Log.d(TAG, "using error stream")
                inputStream = BufferedInputStream(urlConnection.errorStream)
            }
            val bufferSize = 8192
            val inBuffer = ByteArray(bufferSize)
            var numRead: Int
            response = inputStream.bufferedReader().use { it.readText() }
            urlConnection.disconnect()

            clientResponse.response = response
            clientResponse.headers = urlConnection.headerFields
            return clientResponse

        } catch (e: Exception) {
            Log.e(TAG, "Error while reading in from connection", e)
            if (inputStream != null) {
                try {
                    inputStream.close()
                } catch (ex: Exception) {
                }

            }
            urlConnection.disconnect()
            throw HttpClientException(
                "Error while reading in from connection",
                e
            )
        }

    }
}
