package uk.co.city.accountmanager.walmart.sso

import uk.co.city.core.walmart.wmsso.WMUser

/**
 * 10/6/15.
 */
class WMAccountAuthenticationTestImpl : WMAccountAuthentication {

    override var themeColor: String
        get() = "123"
        set(hex) {}

    override var biometricPref: Boolean
        get() = true
        set(biometricPref) {}

    override val response: String
        get() = "123"

    override val env: String
        get() = "123"

    override fun authenticate(user: WMUser, password: String): WMUser {
        user.token = "123456"
        return user
    }

    override fun setAppEnv(env: String) {}

    override fun setPackageId(packageId: String) {}

    override fun isTokenValid(user: WMUser): Boolean {
        return true
    }

    override fun isTokenValidAtServer(user: WMUser): Boolean {
        return true
    }


    override fun invalidate(user: WMUser): Boolean {
        return true
    }
}
