package uk.co.city.accountmanager.walmart.http

/**
 * Created by awhelms on 11/2/15.
 */
class HttpClientException : Exception {
    internal constructor() : super() {}

    internal constructor(detailMessage: String) : super(detailMessage) {}

    internal constructor(detailMessage: String, throwable: Throwable) : super(detailMessage, throwable) {}

    internal constructor(throwable: Throwable) : super(throwable) {}
}
