package uk.co.city.accountmanager.walmart.sso

import android.content.Context

/**
 */
object WMAccountAuthenticationFactory {

    fun getInstance(ctx: Context): WMAccountAuthentication {
        return WMAccountAuthenticationMESImpl(ctx)
    }
}
