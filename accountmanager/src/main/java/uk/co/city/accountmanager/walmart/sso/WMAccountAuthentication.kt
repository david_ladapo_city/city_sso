package uk.co.city.accountmanager.walmart.sso

import uk.co.city.core.walmart.wmsso.WMUser

/**
  */
interface WMAccountAuthentication {

    var biometricPref: Boolean

    var themeColor: String

    val response: String

    /**
     * Returns what enviorment the app is in.
     * @return
     */
    val env: String

    /**
     * Authenticate the given user and password.  Return a WMUser with token populated (at minimum).
     *
     * @param user
     * @param password
     * @return
     */
    fun authenticate(user: WMUser, password: String): WMUser

    /**
     * Determine whether the token in the user object is valid.  This should only use the data in the user object and not perform network activity.
     * @param user
     * @return
     */
    fun isTokenValid(user: WMUser): Boolean

    /**
     * Sets what enviorment the app is in.
     * @param env
     * @return
     */
    fun setAppEnv(env: String)

    /**
     * Sets what the packageId is.
     * @param packageId
     * @return
     */
    fun setPackageId(packageId: String)

    /**
     * Determine whether the token for the user object is valid by performing network activity.
     *
     * @param user
     * @return
     */
    fun isTokenValidAtServer(user: WMUser): Boolean

    /**
     * Invalidate a user token
     *
     * @param user
     * @return
     */
    fun invalidate(user: WMUser): Boolean
}
