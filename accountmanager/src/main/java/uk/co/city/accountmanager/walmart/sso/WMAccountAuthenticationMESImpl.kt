package uk.co.city.accountmanager.walmart.sso

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.provider.Settings.Secure
import android.util.Base64
import uk.co.city.accountmanager.walmart.http.HttpClient
import uk.co.city.accountmanager.walmart.http.HttpClientException
import uk.co.city.accountmanager.walmart.http.HttpClientResponse
import uk.co.city.accountmanager.walmart.resource.ResourceHelper
import uk.co.city.core.walmart.wmsso.WMUser
import uk.co.city.core.walmart.component.CryptoUtil
import org.json.JSONObject
import org.json.JSONTokener
import timber.log.Timber
import uk.co.city.accountmanager.BuildConfig
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

/**
 *  10/30/15.
 */
class WMAccountAuthenticationMESImpl(private val mContext: Context) : WMAccountAuthentication {

    private val res: ResourceHelper

    override val response: String
        get() = responseError

    override val env: String
        get() = appEnv

    override var biometricPref: Boolean
        get() = isBiometricAllowed
        set(biometricPref) {
            isBiometricAllowed = biometricPref
        }


    init {
        res = ResourceHelper(mContext)

    }


    override fun authenticate(user: WMUser, password: String): WMUser {
        var authUser:WMUser = user
        try {
            authUser = authenticate(user, password,
                INTERNAL_AUTH_URL
            )
        } catch (e: Exception) {
            Timber.e( "Exception internal auth: ${e.message}")
        }
        authUser?.let {
            if(it.token != null || it.authenticationState != null){

                if ("FAILED" == it.authenticationState || it.token!!.isEmpty()) {
                    try {
                        authUser = authenticate(it, password,
                            EXTERNAL_AUTH_URL
                        )
                    } catch (e: Exception) {
                        Timber.e( "Exception external auth: ${e.message}")
                    }
                }
            }

        }
        return authUser
    }


    fun getUserHeaders(url: String, password: String, user: WMUser): Map<String, String> {
        var password = password
        var headers = HashMap<String, String>()
        val fmt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateStr = fmt.format(Date())
        val deviceType = Build.MODEL
        val ssoVersion = BuildConfig.VERSION_NAME
        val uuid = Secure.getString(mContext.contentResolver, Secure.ANDROID_ID)


        headers.put("WMT-SVC-ConsumerId", CONSUMER_ID)
        headers.put("WMT-SVC-CountryCode", user?.countryCode!!)
        headers.put("WMT-SVC-Date", dateStr)

        Timber.d("SSO VERSION: $ssoVersion")
        headers.put("WMT-SVC-DeviceVersion", ssoVersion)

        Timber.d("APP PACKAGE ID: $packageName")
        headers.put("WMT-SVC-PackageId", packageName)

        Timber.d("DEVICE UUID: $uuid")
        headers.put("WMT-SVC-DeviceUUID", uuid)

        Timber.d("BUILD TYPE: $deviceType")
        headers.put("WMT-SVC-DeviceType", deviceType)

        Timber.d("APPENV IN HEADER: $appEnv")
        headers.put("WMT-SVC-Env", appEnv)

        headers["WMT-SVC-OSType"] = "Android"

        if (user.siteId != null) {
            headers.put("WMT-SVC-StoreNumber", user.siteId!!.toString())
        }

        try {
            val crypto = CryptoUtil(ENCR_KEY)

            var domain = user.domain
            if (user.domain.equals("store", ignoreCase = true)) {
                password = password.toUpperCase()
            } else if (domain.equals("dc", ignoreCase = true)) {
                domain = "WMSC"
            }

            if (url.contains("homeoffice")) {
                headers.run {
                    put("WMT-SVC-Domain", domain!!)
                    put("WMT-SVC-Pwd", password)
                    put("WMT-SVC-Userid", user.userId!!)
                }
            } else {
                headers.run {
                    put("WMT-SVC-Domain", crypto.encrypt(domain!!))
                    put("WMT-SVC-Pwd", crypto.encrypt(password))
                    put("WMT-SVC-Userid", crypto.encrypt(user.userId!!))
                }

                val signature = "GET$dateStr/AuthenticateUser"
                Timber.d("hashing signature $signature")
                headers.put("WMT-SVC-Hash", hash(signature))
                Timber.d("hash result " + hash(signature))
            }

        } catch (e: Exception) {
            Timber.e("Error while encrypting login components: ${e.message}")
            user.authenticationState = "FAILED"
            user.authenticationResultMsg = res.getString("errorEncrypting")
            headers = HashMap()
        }

        return headers
    }


    fun makeAuthnRequest(url: String, headers: Map<String, String>, user: WMUser): String? {
        val client = HttpClient()
        val clientResponse: HttpClientResponse
        var response: String? = ""
        try {
            Timber.i("Using endpoint $url")
            clientResponse = client.get(url, headers)
            response = clientResponse.response
            Timber.d("Response headers " + clientResponse.headers!!.toString())
            if (clientResponse.headers != null && clientResponse.headers!!["WMT-SVC-ErrMsg"] != null) {
                user.authenticationState = "FAILED"
                user.authenticationResultMsg = clientResponse.headers!!["WMT-SVC-ErrMsg"]!![0]
                responseError = clientResponse.headers!!["WMT-SVC-ErrMsg"]!![0]
            }
        } catch (e: HttpClientException) {
            Timber.e("Http Client error: ${e.message}")
        }

        return response
    }

    override fun setAppEnv(env: String) {
        Timber.d("APP ENV: " + env!!)
        env.let {
            appEnv = when (env.toUpperCase()) {
                "DEV" -> "DEV"
                "QA" -> "QA"
                else -> "PROD"

            }
        }

    }


    override fun setPackageId(packageId: String) {
        packageName = packageId
    }

    override var themeColor: String
        get() = themeColor
        set(hex) {
            if (hex != null) {
                try {
                    val color = Color.parseColor(hex)
                    themeColor = hex
                } catch (e: IllegalArgumentException) {
                    Timber.e("Not a valid hex code.")
                }

            }
        }


    private fun parseAuthnResponse(response: String?, user: WMUser) {
        try {
            if (response != null && response.trim { it <= ' ' }.isNotEmpty()) {
                val jsonResp = JSONTokener(response).nextValue() as JSONObject

                val userAuthResponse = jsonResp.getJSONObject("userAuthResponse")
                val responseMessage = userAuthResponse.getJSONObject("responseMessage")
                Timber.d("Token received from response " + responseMessage.getString("Token"))
                user.token = responseMessage.getString("Token")
                user.authenticationState = null
                val expirationTime: Date
                if (responseMessage.has("validity")) {
                    Timber.d("validity present and set to " + responseMessage.getLong("validity"))
                    user.tokenValidity = responseMessage.getLong("validity")
                    val ms = user.tokenValidity!! * 1000
                    expirationTime = Date(Date().time + ms)
                    user.expiresAt = expirationTime.time
                } else {
                    expirationTime = Date(Date().time + DEFAULT_TOKEN_EXP_MS)
                }
                user.expiresAt = expirationTime.time


                if (responseMessage.has("refreshToken")) {
                    user.refreshToken = responseMessage.getString("refreshToken")
                }

                // process the 'additional' fields.
                // basically anything that is not in the ignoreKeys Set will get added as an
                // additional field to the user.  the ignoreKeys Set contains the keys that should
                // not go in the additional fields because they are either represented by a normal field
                // or are not necessary to be exposed.
                var key: String
                val keys = responseMessage.keys()
                while (keys.hasNext()) {
                    key = keys.next()
                    if (!ignoreKeys.contains(key)) {
                        if (user.additional == null) {
                            user.additional = JSONObject()
                        }

                        user.additional!!.put(key, responseMessage.get(key))
                    }
                }
            }
        } catch (e: Exception) {
            Timber.e("Error while parsing response: ${e.message}")
            user.authenticationState = "FAILED"
            user.authenticationResultMsg = res.getString("errorParsing")
        }

    }

    fun authenticate(user: WMUser, password: String, url: String): WMUser {

        val headers = getUserHeaders(url, password, user)
        if (!headers.isEmpty()) {
            val response = makeAuthnRequest(url, headers, user)
            if (response!!.isEmpty()) {
                user.authenticationState = "FAILED"
                user.authenticationResultMsg = res.getString("errorCommunicating")
            } else {
                parseAuthnResponse(response, user)
            }
        }
        return user
    }

    override fun isTokenValid(user: WMUser): Boolean {
        return if (user.token != null && user.token !== "" && user.expiresAt != null) {
            // current time is before expiration time
            Date().before(Date(user.expiresAt!!))
        } else false
    }

    override fun isTokenValidAtServer(user: WMUser): Boolean {
        // TODO - implement
        return false
    }

    @Throws(NoSuchAlgorithmException::class, InvalidKeyException::class)
    fun hash(input: String): String {
        //        Timber.d("hashing [" + input + "] with key [" + HASH_KEY + "]");
        val secretKeySpec = SecretKeySpec(HASH_KEY.toByteArray(), "HmacSHA256")
        val mac = Mac.getInstance("HmacSHA256")
        mac.init(secretKeySpec)
        val result = mac.doFinal(input.toByteArray())
        return Base64.encodeToString(result, Base64.DEFAULT)
    }


    override fun invalidate(user: WMUser): Boolean {
        var invalidated = false
        try {
            invalidated = invalidate(user,
                INTENRAL_INVALIDATE_URL
            )
        } catch (e: Exception) {
            Timber.e( "Exception internal auth: ${e.message}")
        }

        if (!invalidated) {
            invalidated = invalidate(user,
                EXTERNAL_INVALIDATE_URL
            )
        }
        /* force after the fact? */
        invalidated = true
        return invalidated
    }


    fun getInvalidationHeaders(user: WMUser, url: String): Map<String, String> {
        var headers: MutableMap<String, String> = HashMap()
        var domain: String
        val fmt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dateStr = fmt.format(Date())
        headers.put("WMT-SVC-ConsumerId", CONSUMER_ID)
        user.countryCode?.let { headers.put("WMT-SVC-CountryCode", it) }
        headers.put("WMT-SVC-Date", dateStr)
        headers.put("WMT-SVC-ServiceName", "iam.invalidate")
        headers.put("Content-Type", "application/json")
        user.token?.let { headers.put("WMT-SVC-Usertoken", it) }
        user.token?.let { headers.put("Token", it) }
        user.siteId?.let {
            headers.put("WMT-SVC-StoreNumber", it.toString())
        }

        try {
            val crypto = CryptoUtil(ENCR_KEY)

            domain = user.domain!!
            if (domain.equals("dc", ignoreCase = true)) {
                domain = "WMSC"
            }

            if (url.contains("homeoffice")) {
                headers.put("WMT-SVC-Domain", domain)
                user.userId?.let { headers.put("WMT-SVC-Userid", it) }
            } else {
                headers.put("WMT-SVC-Domain", crypto.encrypt(domain))
                user.userId?.let { crypto.encrypt(it) }?.let { headers.put("WMT-SVC-Userid", it) }

                val signature = "DELETE$dateStr/"
                Timber.d("hashing signature $signature")
                headers.put("WMT-SVC-Hash", hash(signature))
                Timber.d("hash result " + hash(signature))
                Timber.d("Invalidate Headers: $headers")
            }

        } catch (e: Exception) {
            Timber.e("Error while encrypting signOut components: ${e.message}")
            user.authenticationState = "FAILED"
            user.authenticationResultMsg = res.getString("errorEncrypting")
            headers = HashMap()
        }

        return headers
    }


    fun performInvalidation(url: String, headers: Map<String, String>, user: WMUser): String? {
        val client = HttpClient()
        val clientResponse: HttpClientResponse
        var response: String? = ""
        try {
            Timber.i("Using endpoint $url")
            clientResponse = client.delete(url, headers, "{}")
            Timber.d("Client Response: " + clientResponse.response!!)
            response = clientResponse.response
        } catch (e: HttpClientException) {
            Timber.e("Http Client error: ${e.message}")
            user.authenticationState = "FAILED"
            user.authenticationResultMsg = res.getString("errorCommunicating")
        }

        return response
    }


    fun parseResponse(response: String?, user: WMUser): Boolean {
        var invalidated = false
        try {
            if (response != null && response.trim { it <= ' ' }.length > 0) {
                val jsonResp = JSONTokener(response).nextValue() as JSONObject

                Timber.d("INVALIDATE STATUS " + jsonResp.getBoolean("invalidated"))

                invalidated = jsonResp.getBoolean("invalidated")
            }

        } catch (e: Exception) {
            Timber.e("Error while parsing response: ${e.message}")
            user.authenticationState = "FAILED"
            user.authenticationResultMsg = res.getString("errorParsing")
        }

        return invalidated
    }

    fun invalidate(user: WMUser, url: String): Boolean {
        var invalidated = false
        val headers = getInvalidationHeaders(user, url)
        if (!headers.isEmpty()) {
            val response = performInvalidation(url, headers, user)
            if (!response!!.isEmpty()) {
                invalidated = parseResponse(response, user)
            }
        }
        return invalidated
    }

    companion object {

        private val TAG = "AcctAuthMESImpl"

        //    private static final String URL = "https://mobileservices-lab.homeoffice.wal-mart.com/AuthenticateUser";
        private val INTERNAL_AUTH_URL = "https://mobileservices.homeoffice.wal-mart.com/AuthenticateUser"
        private val EXTERNAL_AUTH_URL = "https://mobileservices.wal-mart.com/AuthenticateUser"
        private val INTENRAL_INVALIDATE_URL = "https://mobileservices.homeoffice.wal-mart.com"
        private val EXTERNAL_INVALIDATE_URL = "https://mobileservices.wal-mart.com"

        private val CONSUMER_ID = "C14157119728352"
        private val HASH_KEY = "kKseZBrde5t3HvTCo0IwK2RZH+QK2cNb+KMFC0hbMYQ="
        private val ENCR_KEY = "ebl8toBcThWufSL72KIb7ky9xMqon5rr+EYyTpxnzYM="

        // 8 hours
        private val DEFAULT_TOKEN_EXP_MS = 12 * 60 * 60 * 1000L
        // for testing
        //    private static final Long DEFAULT_TOKEN_EXP_MS = 10 * 1000L;

        private val ignoreKeys = HashSet<String>()
        var appEnv = "PROD"
        lateinit var packageName: String
        var responseError = "A communication error occurred while authenticating."
        var isBiometricAllowed: Boolean = false

        init {
            ignoreKeys.add("code")
            ignoreKeys.add("Token")
            ignoreKeys.add("validity")
            ignoreKeys.add("refreshToken")
        }
    }
}