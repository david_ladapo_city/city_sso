package uk.co.city.accountmanager.walmart.sso

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyPermanentlyInvalidatedException
import android.security.keystore.KeyProperties
import androidx.annotation.RequiresApi
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import androidx.core.os.CancellationSignal
import timber.log.Timber
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import java.security.spec.ECGenParameterSpec
import javax.crypto.KeyGenerator


/**
 11/15/18.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
class WMBiometric @RequiresApi(api = Build.VERSION_CODES.M)
constructor(internal var mContext: Context) {
    internal lateinit var mBiometric: BiometricPrompt
    internal var TAG = "WMSSO-Biometric"
    internal var signature: Signature? = null
    internal lateinit var cancellationSignal: CancellationSignal
    internal var authenticationCallback: BiometricPrompt.AuthenticationCallback? = null
    internal var cancellcationSignal = CancellationSignal()

    private val isSupportBiometricPrompt: Boolean
        get() {
            val packageManager = mContext.packageManager
            return packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)
        }

    private val isSdkVersionSupported: Boolean
        get() {
            Timber.d( "API VERSION: " + Build.VERSION.SDK_INT)
            return Build.VERSION.SDK_INT < 28
        }

    init {
        if (!isSupportBiometricPrompt) {
            Timber.e( "This device does not support Biometric Login")
        } else if (!isSdkVersionSupported) {
            Timber.d( "API VERSION IS 28 or higher.")
            try {
                try {
                    signature = initSignature(KEY_NAME)
                } catch (e: KeyPermanentlyInvalidatedException) {
                    throw RuntimeException(e)
                }

            } catch (e: Exception) {
                throw RuntimeException(e)
            }

            cancellationSignal = getCancellationSignal()
            Timber.d( "Auth: $authenticationCallback")
            Timber.d( "signature: $signature")
            Timber.d( "Cancel Signal $cancellationSignal")
        } else {
            Timber.d( "API VERSION IS 28 or lower.")
            try {
                signature = initSignature(KEY_NAME)
            } catch (e: Exception) {
                throw RuntimeException(e)
            }

        }
    }

    fun biometricPromptAuth(callback: BiometricPrompt.AuthenticationCallback, activity: Activity, isFinished: Boolean) {
        mBiometric = BiometricPrompt.Builder(mContext)
            .setTitle("Use your fingerprint to Sign In")
            .setNegativeButton("Cancel", mContext.mainExecutor, DialogInterface.OnClickListener { dialogInterface, i ->
                Timber.i( "Cancel button clicked")
                if (isFinished)
                    activity.finish()
            })
            .build()

        mBiometric.authenticate(
            BiometricPrompt.CryptoObject(signature!!),
            (cancellationSignal.cancellationSignalObject as android.os.CancellationSignal?)!!,
            mContext.mainExecutor,
            callback
        )
    }

    fun biometricAuth(callback: FingerprintManagerCompat.AuthenticationCallback) {
        val fingerprintManagerCompat = FingerprintManagerCompat.from(mContext)

        fingerprintManagerCompat.authenticate(
            FingerprintManagerCompat.CryptoObject(signature!!),
            0,
            cancellcationSignal,
            callback,
            null
        )
    }

    @Throws(Exception::class)
    private fun initSignature(keyName: String): Signature? {
        val keyPair = getKeyPair(keyName)
        Timber.d( "KEY PAIR: $keyPair $keyPair")
        if (keyPair != null) {
            val signature = Signature.getInstance("SHA256withECDSA")
            signature.initSign(keyPair.private)
            return signature
        }
        return null
    }

    private fun getCancellationSignal(): CancellationSignal {
        // With this cancel signal, we can cancel biometric prompt operation
        val cancellationSignal = CancellationSignal()
        cancellationSignal.setOnCancelListener {
            //handle cancel result
            Timber.i( "Canceled")
        }
        return cancellationSignal
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Throws(Exception::class)
    private fun generateKeyPair(keyName: String, invalidatedByBiometricEnrollment: Boolean): KeyPair {
        val keyPairGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_EC, "AndroidKeyStore")

        val builder = KeyGenParameterSpec.Builder(
            keyName,
            KeyProperties.PURPOSE_SIGN
        )
            .setAlgorithmParameterSpec(ECGenParameterSpec("secp256r1"))
            .setDigests(
                KeyProperties.DIGEST_SHA256,
                KeyProperties.DIGEST_SHA384,
                KeyProperties.DIGEST_SHA512
            )
            // Require the user to authenticate with a biometric to authorize every use of the key
            .setUserAuthenticationRequired(true)
            // Generated keys will be invalidated if the biometric templates are added more to user device
            .setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment)

        keyPairGenerator.initialize(builder.build())

        return keyPairGenerator.generateKeyPair()
    }

    @Throws(Exception::class)
    private fun getKeyPair(keyName: String): KeyPair? {
        val keyStore = KeyStore.getInstance("AndroidKeyStore")
        keyStore.load(null)
        if (keyStore.containsAlias(keyName)) {
            // Get public key
            val publicKey = keyStore.getCertificate(keyName).publicKey
            // Get private key
            val privateKey = keyStore.getKey(keyName, null) as PrivateKey
            // Return a key pair
            return KeyPair(publicKey, privateKey)
        }
        return null
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun generateKey() {
        try {

            val keyStore = KeyStore.getInstance("AndroidKeyStore")
            keyStore.load(null)

            val keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
            keyGenerator.init(
                KeyGenParameterSpec.Builder(KEY_NAME, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build()
            )

            keyGenerator.generateKey()

        } catch (exc: KeyStoreException) {
            exc.printStackTrace()
        } catch (exc: NoSuchAlgorithmException) {
            exc.printStackTrace()
        } catch (exc: NoSuchProviderException) {
            exc.printStackTrace()
        } catch (exc: InvalidAlgorithmParameterException) {
            exc.printStackTrace()
        } catch (exc: CertificateException) {
            exc.printStackTrace()
        } catch (exc: IOException) {
            exc.printStackTrace()
        }

    }

    companion object {

        private val KEY_NAME = "test"
    }
}
