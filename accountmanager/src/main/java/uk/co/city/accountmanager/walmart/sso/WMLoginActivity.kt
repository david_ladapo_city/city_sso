package uk.co.city.accountmanager.walmart.sso

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_wm_account_authenticator.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import uk.co.city.accountmanager.AuthProperties
import uk.co.city.accountmanager.R
import uk.co.city.accountmanager.extensions.*
import uk.co.city.accountmanager.utils.NetUtils
import uk.co.city.accountmanager.utils.NetUtils.isFingerprintAvailable
import uk.co.city.accountmanager.utils.NetUtils.isHardwareSupported
import uk.co.city.accountmanager.utils.NetUtils.isNetworkAvailable
import uk.co.city.accountmanager.utils.NetUtils.isSdkVersionSupported
import uk.co.city.accountmanager.walmart.resource.JSONAssetReader
import uk.co.city.core.walmart.wmsso.SSOConstants
import uk.co.city.core.walmart.wmsso.SSOIntentType
import uk.co.city.core.walmart.wmsso.WMUser
import uk.co.city.core.walmart.wmsso.WMUserAdapter
import java.util.*

/**
 */
class WMLoginActivity : LoginAbstractActivity() {


    private var useridField: EditText? = null
    private var passwordField: EditText? = null
    private var countryDropdown: Spinner? = null
    private var domainDropdown: Spinner? = null
    private var subDomainDropdown: Spinner? = null
    private var siteIdField: EditText? = null
    private var progressBarContainer: FrameLayout? = null
    private var banner: FrameLayout? = null
    private var loginForm: LinearLayout? = null
    private var isQa: TextView? = null

    private val mContext = this

    /** Added for shared dropdown list  */
    private var sharedDropdownListData: Map<String, List<Map<String, String>>>? = null
    private var dropdownListData: MutableMap<String, List<Map<String, String>>>? = null
    internal var index = 0
    internal var isValidChar = true

    data class WalmartAuthProperties constructor(
        val userid: String,
        val domain: String?,
        val subDomain: String?,
        val country: String?,
        val siteId: String,
        var password: String
    ) : AuthProperties()




    override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        //TODO re-integrate fabric
        //Fabric.with(this, Crashlytics())

        startTime = System.currentTimeMillis()
        accountStore= WMAccountStoreShardPrefsImpl(applicationContext)
        auth = WMAccountAuthenticationFactory.getInstance(this)

        val appEnv = auth!!.env
        //   val themeColor = auth!!.themeColor
        replyTo = intent.getParcelableExtra(SSOConstants.KEY_REPLY_TO)

        Timber.d("Network available: ${NetUtils.isNetworkAvailable(this)}")

        setContentView(R.layout.activity_wm_account_authenticator)

        // set up icon
        val ssoCustomFont = Typeface.createFromAsset(assets, "SSO-custom.ttf")

        setupIcon(ssoCustomFont)

        val signIn = acc_sign_in
        signIn.setOnClickListener { authenticate() }

        useridField = acc_userid
        passwordField = acc_password
        domainDropdown = acc_domain
        subDomainDropdown = acc_sub_domain
        countryDropdown = acc_country
        siteIdField = acc_site_id
        progressBarContainer = acc_progressBarContainer
        banner = acc_banner
        loginForm = acc_loginForm
        isQa = acc_qaTextView
        fingerprintImg = acc_fingerprint
        settings = acc_settings //When clicked when prompt the user if they want to clear the biometric option

        initFields(appEnv)
        setEnabledFields(true)
        initFingerprint()
        initSettings()
        initBiometricDialog()
        populateDropdownLists()
        initDomain()
        loadLastUserData()
    }

    private fun initFields(appEnv: String) {
        isQa!!.visibility = View.GONE
        if (appEnv == "DEV" || appEnv == "QA") {
            isQa!!.visibility = View.VISIBLE
        }
        if (appEnv == "DEV" || appEnv == "QA") {
            banner!!.setBackgroundColor(Color.parseColor("#42f462"))
        } else {
            banner!!.setBackgroundColor(Color.parseColor("#ff3e98cd"))
        }
        loginForm!!.setOnClickListener { v ->
            v.clearFocus()
            hideKeyboard()
        }
        choseLogonType()
    }

    private fun choseLogonType() {
        val hasBiometricPrefs = auth!!.biometricPref
        val isBiometricSaved = sharedPref.getUserBiometricPref(mContext)
        val isHardwareSupported = isHardwareSupported(mContext)
        val isFPAvail = isFingerprintAvailable(mContext)
        val hasUserBiometricInfo = sharedPref.getUserBiometricInfo(mContext)

        if (hasBiometricPrefs && hasBiometricPrefs && isHardwareSupported && isFPAvail) {
            //Will only show if user has logged in before and accepted to use biometric
            fingerprintImg!!.visibility = View.VISIBLE

        }
        if (hasBiometricPrefs && !isBiometricSaved && isHardwareSupported && isFPAvail) {
            //Will only show if user has logged in before and accepted to use biometric
            settings!!.visibility = View.VISIBLE
        }
    }

    private fun initDomain() {
        domainDropdown!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                subDomainDropdown!!.visibility = View.GONE
                siteIdField!!.visibility = View.GONE
                val selectedDomain = (domainDropdown!!.getItemAtPosition(position) as Map<String, String>)["id"]
                if (selectedDomain!!.equals("store", ignoreCase = true)) {
                    siteIdField!!.visibility = View.VISIBLE
                    siteIdField!!.setHint(R.string.storeclub)
                    /** Dynamic hint setting  */
                } else if ("homeoffice".equals(selectedDomain, ignoreCase = true)) {
                    siteIdField!!.visibility = View.VISIBLE
                    siteIdField!!.setHint(R.string.storeclub)
                } else if (selectedDomain.equals("dc", ignoreCase = true)) {
                    siteIdField!!.visibility = View.VISIBLE
                    siteIdField!!.setHint(R.string.site)
                    subDomainDropdown!!.visibility = View.VISIBLE
                } else if ("vendor".equals(selectedDomain, ignoreCase = true)) {
                    siteIdField!!.setHint(R.string.storeclub)
                    siteIdField!!.visibility = View.VISIBLE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Do nothing
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun initBiometricDialog() {
        if (auth!!.biometricPref && sharedPref.getUserBiometricInfo(mContext) && isFingerprintAvailable(mContext)) {
            //gets called on create instantly if user has opted in before, and user has a fingerprint registered on their devices
            val wmBiometric = WMBiometric(mContext)
            //Biometric dialog needs to be passed in the context, a layout inflater, the current activity, and the callback information for fingerprint compact
            biometricDialog = BiometricDialog(layoutInflater, this@WMLoginActivity, wmBiometric.cancellcationSignal)
            biometricDialog.setFinished(false)

            if (!isSdkVersionSupported) {
                // For Android Pie and above
                //Biometric Prompt auth; need to pass in a callback, activity, and if it's the first biometric login
                wmBiometric.biometricPromptAuth(authenticationCallback, this@WMLoginActivity, false)
            } else {
                //API version under Android Pie
                wmBiometric.biometricAuth(authenticationCallbackLow) //Fingerprint compact auth; need to pass in a callback
                biometricDialog.showDialog()  //custom dialog needs to be show for fingerprint compact
            }
        }
    }

    private fun initSettings() {
        settings!!.setOnClickListener {
            //onClick listener for settings image
            showClearBiometricDialog(this, sharedPref, fingerprintImg!!, settings!!)
            sharedPref
        }
    }


    private fun setupIcon(ssoCustomFont: Typeface?) {
        val icon = acc_icon
        icon.text = SSOIconCharCode.StrokeCircleSolidKey.code()
        icon.typeface = ssoCustomFont
        icon.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        icon.setTextSize(TypedValue.COMPLEX_UNIT_SP, 256f)
    }



    override fun populateDropdownLists() {
        val assetReader = JSONAssetReader()
        /*
        “dropdown_lists_shared.json” contains common labels/data(country dropdown) for all languages .
        Below code gets the language code from device and looks for exist translation json file.
        If file is not present then it will take English translation “dropdown_lists.json” as a default language.
         */
        sharedDropdownListData = assetReader.getKeyValuePairResources(this, "dropdown_lists_shared.json")
        dropdownListData = assetReader.getKeyValuePairResources(this, "dropdown_lists.$lang.json")
        if (dropdownListData!!.isEmpty()) {
            dropdownListData = assetReader.getKeyValuePairResources(this, "dropdown_lists.json")
        }
        dropdownListData!!.putAll(sharedDropdownListData!!)

        populateDropdownList(domainDropdown, dropdownListData!!["domains"])
        populateDropdownList(subDomainDropdown, dropdownListData!!["subDomains"])
        populateDropdownList(countryDropdown, dropdownListData!!["countries"])
    }

    private fun populateDropdownList(dropdownList: Spinner?, nameDesc: List<Map<String, String>>?) {
        if (dropdownList != null && nameDesc != null) {
            val from = arrayOf("desc")
            val to = intArrayOf(android.R.id.text1)

            val simpleAdapter = SimpleAdapter(
                this, nameDesc,
                android.R.layout.simple_spinner_item, from, to
            )
            simpleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            val viewBinder = SimpleAdapter.ViewBinder { view, data, textRepresentation ->
                val textView = view as TextView
                textView.text = textRepresentation
                true
            }
            simpleAdapter.viewBinder = viewBinder
            dropdownList.adapter = simpleAdapter
        }
    }

    override fun updateOnAuthSuccess() {
        val crypto = WMCryptoKeystoreImpl()
        crypto.setContext(this)
        useridField!!.setText(crypto.decrypt(sharedPref.getUsername(this)))
        passwordField!!.setText(crypto.decrypt(sharedPref.getPassword(this)))
        siteIdField!!.setText(sharedPref.getSiteId(this))
        setDropdownValue(domainDropdown, "domains", sharedPref.getDomain(this))
        authenticate()
    }
    override fun onBackPressed() {
        // ignore
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun loadLastUserData() {

        accountStore.getUser(object : WMAccountStore.GetUserResult {
            override fun onSuccess(lastUser: WMUser?) {
                lastUser?.let { user ->
                    useridField!!.setText(lastUser.userId)

                    lastUser.domain?.let {
                        setDropdownValue(domainDropdown, "domains", it)
                    }
                    lastUser.subDomain?.let {
                        setDropdownValue(subDomainDropdown, "subDomains", it)
                    }
                    lastUser.siteId?.let {
                        siteIdField!!.setText(it.toString())
                    }
                    lastUser.countryCode?.let {
                        setDropdownValue(countryDropdown, "countries", it)
                    }
                }
            }


            override fun onError(t: Throwable) {
                Timber.d("Error getting user to load last user details")
            }
        })

    }

    private fun setDropdownValue(dropdownList: Spinner?, listDataKey: String, idValue: String) {
        var i = 0
        while (i < dropdownListData!![listDataKey]!!.size) {
            if (dropdownListData!![listDataKey]!![i]["id"]!!.equals(idValue, ignoreCase = true)) {
                break
            }
            i++
        }
        dropdownList!!.setSelection(i)
    }

    override fun onResume() {
        super.onResume()

        // check if user got updated from a different app
        accountStore.getUser(object : WMAccountStore.GetUserResult {
            override fun onSuccess(user: WMUser?) {
                if (user != null && auth!!.isTokenValid(user)) {
                    WMAccountManagerService.respondWithUser(replyTo, user)
                    finish()
                }
            }

            override fun onError(t: Throwable) {
                Timber.d("Error getting user:  ${t.message}")
            }
        })

    }

    public override fun onStop() {
        super.onStop()
        finish()
    }

    fun validCharacters(userId: String) {
        if (index < invalidCharacters.size) {
            if (userId.contains(invalidCharacters[index])) {
                isValidChar = false
            } else {
                index++
                validCharacters(userId)
            }
        }
        index = 0
    }

    fun inputsAreValid(): Boolean {
        var msg = resources!!.getString(R.string.pleaseFillInFields) + ":\n\n"
        var valid: Boolean? = true
        isValidChar = true


        val triple = validateUserId(msg, valid)
        val userid = triple.first
        msg = triple.second
        valid = triple.third

        validCharacters(userid.toLowerCase())
        Timber.d("VALID CHARS: $isValidChar")
        Timber.d("USER ID: $userid")
        if (!isValidChar && NetUtils.isNetworkAvailable(this)) {
            valid = false
            showAlert(getString(R.string.invalidChar), getString(R.string.invalidCharacters))

        }


        val pair1 = validatePassword(msg, valid)
        msg = pair1.first
        valid = pair1.second

        val pair = checkValidDomains(msg, valid)
        msg = pair.first
        valid = pair.second

        checkValidWithNetwork(isValidChar, valid, msg)
        return valid!! && NetUtils.isNetworkAvailable(this)
    }

    private fun validatePassword(msg: String, valid: Boolean?): Pair<String, Boolean?> {
        var msg1 = msg
        var valid1 = valid
        val password = passwordField!!.text.toString()
        if (password.trim { it <= ' ' }.isEmpty()) {
            msg1 += "- " + getString(R.string.password) + "\n"
            valid1 = false
        }
        return Pair(msg1, valid1)
    }

    private fun validateUserId(
        msg: String,
        valid: Boolean?
    ): Triple<String, String, Boolean?> {
        var msg1 = msg
        var valid1 = valid
        val userid = useridField!!.text.toString()
        if (userid.trim { it <= ' ' }.isEmpty()) {
            msg1 += "- " + getString(R.string.userid) + "\n"
            valid1 = false
        }
        return Triple(userid, msg1, valid1)
    }

    private fun checkValidDomains(
        msg: String,
        valid: Boolean?
    ): Pair<String, Boolean?> {
        var msg1 = msg
        var valid1 = valid
        val domain = if (domainDropdown!!.selectedItem != null)
            (domainDropdown!!.selectedItem as Map<String, String>)["id"]
        else
            ""
        val subDomain = if (subDomainDropdown!!.selectedItem != null)
            (subDomainDropdown!!.selectedItem as Map<String, String>)["id"]
        else
            ""
        val country = if (countryDropdown!!.selectedItem != null)
            (countryDropdown!!.selectedItem as Map<String, String>)["id"]
        else
            ""
        val siteId = siteIdField!!.text.toString()

        if (domain!!.isEmpty()) {
            msg1 += "- " + resources!!.getString(R.string.domain) + "\n"
            valid1 = false
        } else if (domain.equals(SSOConstants.STORE_DOMAIN, ignoreCase = true)) {
            if (siteId.trim { it <= ' ' }.isEmpty()) {
                msg1 += "- " + getString(R.string.storeclub) + "\n"
                valid1 = false
            }
        } else if (domain.equals(SSOConstants.DC_DOMAIN, ignoreCase = true)) {
            if (siteId.trim { it <= ' ' }.isEmpty()) {
                msg1 += "- " + getString(R.string.site) + "\n" // Added site for internalisation
                valid1 = false
            }
            if (subDomain!!.isEmpty()) {
                msg1 += "- " + getString(R.string.password) + "\n"
                valid1 = false
            }
        }
        return Pair(msg1, valid1)
    }


    private fun setEnabledFields(isEnabled: Boolean) {
        useridField!!.isEnabled = isEnabled
        passwordField!!.isEnabled = isEnabled
        domainDropdown!!.isEnabled = isEnabled
        subDomainDropdown!!.isEnabled = isEnabled
        countryDropdown!!.isEnabled = isEnabled
        siteIdField!!.isEnabled = isEnabled
        fingerprintImg!!.isEnabled = isEnabled
        settings!!.isEnabled = isEnabled
    }


    @SuppressLint("StaticFieldLeak")
    private fun authenticate() {

        if (inputsAreValid()) {
            val userid = useridField!!.text.toString()
            val password = passwordField!!.text.toString()
            val domain = if (domainDropdown!!.selectedItem != null)
                (domainDropdown!!.selectedItem as Map<String, String>)["id"]
            else
                ""
            val subDomain = if (subDomainDropdown!!.selectedItem != null)
                (subDomainDropdown!!.selectedItem as Map<String, String>)["id"]
            else
                ""
            val country = if (countryDropdown!!.selectedItem != null)
                (countryDropdown!!.selectedItem as Map<String, String>)["id"]
            else
                ""
            val siteId = siteIdField!!.text.toString()

            progressBarContainer!!.visibility = View.VISIBLE
            setEnabledFields(false)
            val walmart = WalmartAuthProperties(userid, domain, subDomain, country, siteId, password)
            GlobalScope.launch(Dispatchers.Main) {
                lateinit var user: WMUser
                withContext(Dispatchers.Default) {
                    user = authenticateUser(walmart)
                }
                user?.let {
                    val crypto = WMCryptoKeystoreImpl()
                    crypto.setContext(mContext)
                    user.authenticationState?.let {

                        if (it.isNotEmpty()) {
                            processAuthenticateState()
                        } else {
                            accountStore.getUser(getUserResult(user, crypto, walmart))
                        }
                    } ?: accountStore.getUser(getUserResult(user, crypto, walmart))
                }
            }

        }
    }


    private fun authenticateUser(walmart: WalmartAuthProperties): WMUser {
        var user = WMUser()
        user.userId = walmart.userid
        user.domain = walmart.domain
        user.subDomain = walmart.subDomain
        user.countryCode = walmart.country
        if (walmart.siteId.trim { it <= ' ' } != "") {
            user.siteId = java.lang.Long.parseLong(walmart.siteId)
        } else {
            user.siteId = null
        }
        user = auth!!.authenticate(user, walmart.password)
        Timber.d("AWH ---- authenticated user; token exipres at " + user)
        return user
    }

    private fun processAuthenticateState() {
        progressBarContainer!!.visibility = View.GONE
        var msg = auth!!.response
        setEnabledFields(true)
        if (isNetworkAvailable(mContext))
        // added error message in string.xml
            msg = msg.replace("\\s".toRegex(), "")
        val id = resources!!.getIdentifier(msg, "string", packageName)
        if (id != 0) {
            showAlert(getString(R.string.errorTitle), msg)
        } else {
            //Unknown Error will be shown for msg not found
            showAlert(getString(R.string.errorTitle), getString(R.string.unknownerrors))
        }
    }

    private fun getUserResult(
        user: WMUser,
        crypto: WMCryptoKeystoreImpl,
        walmart: WalmartAuthProperties
    ): WMAccountStore.GetUserResult {
        return object : WMAccountStore.GetUserResult {
            override fun onSuccess(orig: WMUser?) {
                accountStore.saveUser(
                    user,
                    saveUserResult(user, orig, crypto, walmart)
                )
            }

            override fun onError(t: Throwable) {
                // TODO do something here
                Timber.e("error getting previous user:  ${t.message}")
            }
        }
    }

    private fun saveUserResult(
        user: WMUser,
        orig: WMUser?,
        crypto: WMCryptoKeystoreImpl,
        walmart: WalmartAuthProperties
    ): WMAccountStore.SaveUserResult {
        return object :
            WMAccountStore.SaveUserResult {
            override fun onSuccess() {
                accountStore.saveUser(user, saveUserObject(user, orig, crypto, walmart))
            }

            override fun onError(t: Throwable) {
                // TODO do something here too
                Timber.e("error saving user:  ${t.message}")

            }
        }
    }

    private fun saveUserObject(
        user: WMUser,
        orig: WMUser?,
        crypto: WMCryptoKeystoreImpl,
        walmart: WalmartAuthProperties
    ): WMAccountStore.SaveUserResult {
        return object :
            WMAccountStore.SaveUserResult {
            @TargetApi(Build.VERSION_CODES.M)
            override fun onSuccess() {
                WMAccountManagerService.respondWithUser(replyTo, user)
                checkUserChangedAndNotify(orig, user)

                Timber.d("FROM AUTH: " + auth!!.biometricPref)

                storeUser(crypto, walmart)
                val isHardWareCapable =
                    auth!!.biometricPref && isHardwareSupported(mContext)
                val isFingerprintAvail = isFingerprintAvailable(mContext)
                if (!(mContext as Activity).isFinishing && isHardWareCapable && isFingerprintAvail)
                //After successful authentication, check if the user has been asked for biometric is false, they have the hardware needed, and they have a saved fingerprint on their device
                    if (sharedPref.getUserBiometricPref(mContext)) {
                        createBiometricDialog(crypto, walmart)
                    } else {
                        finish()
                    }
                else {
                    finish()
                }
            }

            override fun onError(t: Throwable) {
                // TODO do something here too
                Timber.e("error saving user:  ${t.message}")

            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @TargetApi(Build.VERSION_CODES.M)
    private fun createBiometricDialog(
        crypto: WMCryptoKeystoreImpl,
        walmart: WalmartAuthProperties
    ) {
        val builder = AlertDialog.Builder(mContext)
        builder.setCancelable(true)
        builder.setTitle(R.string.use_biometric_question)
        builder.setMessage(R.string.allow_sso_signon)
        builder.setPositiveButton(
            getString(R.string.confirm)
        ) { dialog, which ->

            biometricDialog = showBiometricDialog(crypto, walmart, sharedPref)
        }
        builder.setNegativeButton(android.R.string.cancel) { dialog, which ->
            //If user doesn't want to use biometric optIn is set to false
            sharedPref.setUserBiometricInfo(false, mContext)
            finish()
        }

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
        //After initial user will no longer be asked if they want to use biometric unless settings are reset
        sharedPref.setUserBiometricPref(false, mContext)
    }


    private fun storeUser(crypto: WMCryptoKeystoreImpl, walmart: WalmartAuthProperties) {
        val decryptedUser = crypto.decrypt(sharedPref.getUsername(mContext))
        if (sharedPref.getUserBiometricInfo(mContext) && walmart.userid == decryptedUser) {
            //Handling password changes
            Timber.d("Updating User...Encrypting and storing the user's login information")
            domain?.let {
                country?.let {
                    sharedPref.storeUserCreds(
                        crypto.encrypt(walmart.userid)!!,
                        crypto.encrypt(walmart.password)!!,
                        walmart.domain!!,
                        walmart.siteId,
                        walmart.country!!,
                        mContext
                    )
                }
            }

        }
    }


    private fun checkUserChangedAndNotify(orig: WMUser?, updated: WMUser) {
        val changedFields = ArrayList<String>()

        if (orig != null) {
            Timber.d("comparing user entries.")
            Timber.d("original $orig")
            Timber.d("updated $updated")
            if (orig.userId != updated.userId) {
                changedFields.add(WMUserAdapter.USERID)
            }

            if (orig.domain != updated.domain) {
                changedFields.add(WMUserAdapter.DOMAIN)
            } else if (updated.domain == "DC") {
                if (orig.subDomain != updated.subDomain) {
                    changedFields.add(WMUserAdapter.SUB_DOMAIN)
                }

                if (orig.siteId != updated.siteId) {
                    changedFields.add(WMUserAdapter.SITE)
                }
            } else if (updated.domain == "STORE") {
                if (orig.siteId != updated.siteId) {
                    changedFields.add(WMUserAdapter.SITE)
                }
            }

            if (orig.token == null || orig.token != updated.token) {
                changedFields.add(WMUserAdapter.TOKEN)
            }
        }

        if (orig == null || changedFields.size > 0) {
            val i = Intent(SSOIntentType.UserChanged.action)
            i.putExtras(WMUserAdapter.toBundle(updated))
            i.putStringArrayListExtra(SSOConstants.EXTRA_CHANGED_FIELDS, changedFields)

            Timber.d("Notifying user changed " + SSOIntentType.UserChanged.action)
            sendBroadcast(i, SSOConstants.PERM_RECV_USER_CHANGED)
        }
    }


    companion object {
        var startTime: Long = 0
    }
}
