package uk.co.city.accountmanager.walmart.sso

import android.app.Service
import android.content.Intent
import android.os.IBinder
import timber.log.Timber

/**
 */
class WMAccountService : Service() {
    override fun onBind(intent: Intent): IBinder? {
        Timber.d( "The package name for this service .........is $packageName")
        val authenticator = WMAccountAuthenticator(this)
        return authenticator.iBinder
    }
}
