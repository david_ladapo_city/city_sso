package uk.co.city.accountmanager.walmart.sso

import android.content.Context
import timber.log.Timber

/**
 * Created by Adithya Sreekumar on 11/15/18.
 */

class WMBiometricSharedPref {

    private val walmatSsoApp = "com.com.walmart.sso.app"

    internal var isOptInKey: Boolean = false
    internal var firstPromptBiometric: Boolean = false

    init {
        Timber.d( "User want biometric: $isOptInKey")
        Timber.d( "User ask if want biometric $firstPromptBiometric")
    }

    fun getUserBiometricInfo(mContext: Context): Boolean {
        val prefs = mContext.getSharedPreferences(
            walmatSsoApp, Context.MODE_PRIVATE
        )
        return prefs.getBoolean("isOptIn", false)
    }

    fun setUserBiometricInfo(isOptIn: Boolean, mContext: Context) {
        val prefs = mContext.getSharedPreferences(
            walmatSsoApp, Context.MODE_PRIVATE
        )
        isOptInKey = isOptIn
        prefs.edit().putBoolean("isOptIn", isOptIn).apply()
    }

    fun getUserBiometricPref(mContext: Context): Boolean {
        val prefs = mContext.getSharedPreferences(
            walmatSsoApp, Context.MODE_PRIVATE
        )
        return prefs.getBoolean("bioPref", true)
    }

    fun setUserBiometricPref(bioPref: Boolean, mContext: Context) {
        val prefs = mContext.getSharedPreferences(
            walmatSsoApp, Context.MODE_PRIVATE
        )
        firstPromptBiometric = bioPref
        prefs.edit().putBoolean("bioPref", bioPref).apply()
    }

    fun storeUserCreds(
        userName: String,
        password: String,
        domain: String,
        siteId: String,
        countryCode: String,
        mContext: Context
    ) {
        val prefs = mContext.getSharedPreferences(
            walmatSsoApp, Context.MODE_PRIVATE
        )
        prefs.edit().putString("username", userName).apply()
        prefs.edit().putString("password", password).apply()
        prefs.edit().putString("domain", domain).apply()
        prefs.edit().putString("siteId", siteId).apply()
        prefs.edit().putString("countryCode", countryCode).apply()
    }

    fun getUsername(mContext: Context): String {
        val prefs = mContext.getSharedPreferences(
            walmatSsoApp, Context.MODE_PRIVATE
        )
        return prefs.getString("username", "")
    }

    fun getPassword(mContext: Context): String {
        val prefs = mContext.getSharedPreferences(
            walmatSsoApp, Context.MODE_PRIVATE
        )
        return prefs.getString("password", "")
    }

    fun getDomain(mContext: Context): String {
        val prefs = mContext.getSharedPreferences(
            walmatSsoApp, Context.MODE_PRIVATE
        )
        return prefs.getString("domain", "")
    }

    fun getSiteId(mContext: Context): String {
        val prefs = mContext.getSharedPreferences(
            walmatSsoApp, Context.MODE_PRIVATE
        )
        return prefs.getString("siteId", "")
    }

    fun getCountryCode(mContext: Context): String {
        val prefs = mContext.getSharedPreferences(
            walmatSsoApp, Context.MODE_PRIVATE
        )
        return prefs.getString("countryCode", "")
    }
}
