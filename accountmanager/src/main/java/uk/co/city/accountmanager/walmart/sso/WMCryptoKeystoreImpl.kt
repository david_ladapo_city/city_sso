package uk.co.city.accountmanager.walmart.sso

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.security.KeyPairGeneratorSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import androidx.annotation.RequiresApi
import timber.log.Timber
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.math.BigInteger
import java.security.*
import java.util.*
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream
import javax.crypto.spec.SecretKeySpec
import javax.security.auth.x500.X500Principal

/**

 */
class WMCryptoKeystoreImpl : WMCrypto {

    private var context: Context? = null

    private val secretKey: Key
        get() {
            val encryptedAesKey = aesKey
            val encryptedKey = Base64.decode(encryptedAesKey, Base64.DEFAULT)
            val key = rsaDecrypt(encryptedKey)
            return SecretKeySpec(key, "AES")
        }

    private val aesKey: String
        get() {
            val prefs = context!!.getSharedPreferences(PREFS, Context.MODE_PRIVATE)
            var encryptedKey = prefs.getString(AES_KEY, null)
            if (encryptedKey == null) {
                val key = ByteArray(16)
                val secureRandom = SecureRandom()
                secureRandom.nextBytes(key)

                val encrypted = rsaEncrypt(key)
                encryptedKey = Base64.encodeToString(encrypted, Base64.DEFAULT)
                val editor = prefs.edit()
                editor.putString(AES_KEY, encryptedKey)
                editor.commit()
            }
            return encryptedKey
        }

    private val keyStore: KeyStore?
        get() {

            var keyStore: KeyStore? = null
            try {
                keyStore = KeyStore.getInstance(PROVIDER)
                keyStore!!.load(null)
                if (!keyStore.containsAlias(ALIAS)) {
                    generate()
                }
            } catch (e: Exception) {
                Timber.e("Caught ${e.javaClass.name} getting keystore: ${e.message}")
            }

            return keyStore
        }

    override fun setContext(context: Context) {
        this.context = context
    }

    override fun encrypt(toEncrypt: String?): String? {
        context?.let { _ ->
            toEncrypt?.let {
                try {
                    val c = Cipher.getInstance(AES_MODE, "BC")
                    c.init(Cipher.ENCRYPT_MODE, secretKey)
                    val encodedBytes = c.doFinal(it.toByteArray(charset("UTF-8")))
                    return Base64.encodeToString(encodedBytes, Base64.DEFAULT)
                } catch (e: Exception) {
                    Timber.e("Caught ${e.javaClass.name}  encrypting: ${e.message}")
                }

            }?: Timber.w("string to encrypt  cannot blank or null")
        }?: Timber.w("context not set!! cannot encrypt")

        return null
    }

    override fun decrypt(encrypted: String?): String? {
        context?.let { _ ->
            encrypted?.let {
                try {
                    val c = Cipher.getInstance(AES_MODE, "BC")
                    c.init(Cipher.DECRYPT_MODE, secretKey)
                    val decodedBytes = c.doFinal(Base64.decode(it.toByteArray(charset("UTF-8")), Base64.DEFAULT))
                    return String(decodedBytes, charset("UTF-8"))
                } catch (e: Exception) {
                    Timber.e("Caught ${e.javaClass.name}  while decrypting: ${e.message}")
                }
            } ?: Timber.w("Encrypted string cannot be blank or null")
        } ?: Timber.w("context not set!! cannot decrypt")
        return null
    }

    private fun rsaEncrypt(toEncrypt: ByteArray): ByteArray? {
        context?.let {_->
            keyStore?.let { rsaKeystore ->
                try {
                    val input = Cipher.getInstance(RSA_MODE)
                    input.init(Cipher.ENCRYPT_MODE, rsaKeystore.getCertificate(ALIAS).publicKey)

                    val outputStream = ByteArrayOutputStream()
                    val cipherOutputStream = CipherOutputStream(outputStream, input)
                    cipherOutputStream.write(toEncrypt)
                    cipherOutputStream.close()

                    return outputStream.toByteArray()
                } catch (e: Exception) {
                    Timber.e("Caught ${e.javaClass.name}  while encrypting: ${e.message}")
                }
            }
        } ?: Timber.w("context not set; cannot encrypt")

        return null
    }

    private fun rsaDecrypt(encrypted: ByteArray): ByteArray? {
        context?.let {
            keyStore?.let { rsaKeystore ->
                try {
                    val privateKey = rsaKeystore.getKey(ALIAS, null) as PrivateKey
                    val output = Cipher.getInstance(RSA_MODE)
                    output.init(Cipher.DECRYPT_MODE, privateKey)
                    val cipherInputStream = CipherInputStream(ByteArrayInputStream(encrypted), output)
                    var bytes = cipherInputStream.readBytes()
                    //                    String decrypted = new String(bytes, 0, bytes.length, "UTF-8");
                    //                    Timber.d( "decrypted " + encrypted + " to " + decrypted);
                    return bytes
                } catch (e: Exception) {
                    Timber.e("Caught ${e.javaClass.name}  while encrypting: ${e.message}")
                }
            }

        } ?: Timber.w("context not set; cannot decrypt")
        return null
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private fun generate() {
        try {
            val start = Calendar.getInstance()
            val end = Calendar.getInstance()
            end.add(Calendar.YEAR, 30)
            val generator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA,
                PROVIDER
            )
            val builder = getKeySpecBuilder(start, end)
            val spec = builder.build()
            generator.initialize(spec)
            val keyPair = generator.generateKeyPair()
        } catch (e: NoSuchAlgorithmException) {
            Timber.e("Caught NoSuchAlgorithmException while trying to generate key: ${e.message}")
        } catch (e: NoSuchProviderException) {
            Timber.e("Caught NoSuchProviderException while trying to generate key: ${e.message}")
        } catch (e: InvalidAlgorithmParameterException) {
            Timber.e("Caught InvalidAlgorithmParameterException while trying to generate key: ${e.message}")
        }

    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun getKeySpecBuilder(
        start: Calendar,
        end: Calendar
    ): KeyPairGeneratorSpec.Builder {
        return KeyPairGeneratorSpec.Builder(context!!)
            .setAlias(ALIAS)
            .setKeyType(KeyProperties.KEY_ALGORITHM_RSA)
            .setSubject(X500Principal("CN=WMAccountManager, O=Walmart Stores Inc."))
            .setSerialNumber(BigInteger.ONE)
            .setStartDate(start.time)
            .setEndDate(end.time)
    }

    companion object {
        private val ALIAS = "WMAccountKey"
        private val PROVIDER = "AndroidKeyStore"
        private val RSA_MODE = "RSA/ECB/PKCS1Padding"
        private val AES_MODE = "AES/ECB/PKCS7Padding"

        private val PREFS = "WMCrypto"
        private val AES_KEY = "aesKey"
    }

}
