package uk.co.city.accountmanager.walmart.sso

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class MyReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "onReceive() $intent")
        if (intent.extras!!.size() > 0) {
            Log.d(TAG, "extras " + intent.extras!!)
        }
    }

    companion object {
        private val TAG = "MyReceiver"
    }
}
