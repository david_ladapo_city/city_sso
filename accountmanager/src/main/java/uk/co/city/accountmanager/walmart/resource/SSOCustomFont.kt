package uk.co.city.accountmanager.walmart.resource

import android.app.Activity
import android.graphics.Typeface

/**
 *  on 12/17/15.
 */
class SSOCustomFont(activity: Activity) {
    init {
        loadCustomFont(activity)
    }

    private fun loadCustomFont(activity: Activity) {
        val typeface = Typeface.createFromAsset(activity.assets, "SSO-custom.ttf")
    }


}
