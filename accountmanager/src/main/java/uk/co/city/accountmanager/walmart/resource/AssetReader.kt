package uk.co.city.accountmanager.walmart.resource

import android.app.Activity

/**
 *  on 10/27/15.
 */
interface AssetReader {
    fun getKeyValuePairResources(activity: Activity, filename: String): Map<String, List<Map<String, String>>>
}
