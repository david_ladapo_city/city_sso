package uk.co.city.accountmanager.walmart.sso

import android.annotation.TargetApi
import android.app.Activity
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.os.Messenger
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import timber.log.Timber
import uk.co.city.accountmanager.utils.NetUtils
import java.util.*

abstract class LoginAbstractActivity : AppCompatActivity() {
    protected var fingerprintImg: ImageView? = null
    protected var settings: ImageView? = null
    protected lateinit var biometricDialog: BiometricDialog
    protected lateinit var accountStore:WMAccountStore
    protected val sharedPref = WMBiometricSharedPref()
    protected var auth: WMAccountAuthentication? = null
    protected var replyTo: Messenger? = null
    // To get the language code from device
    protected val lang = Locale.getDefault().language



    protected abstract fun populateDropdownLists()
    protected abstract fun updateOnAuthSuccess()
    // Callback for biometric authentication result
    protected val authenticationCallback: BiometricPrompt.AuthenticationCallback
        get() {

            return object : BiometricPrompt.AuthenticationCallback() {

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    updateOnAuthSuccess()
                    Timber.d("BIO AUTH SUCCESS")
                }
            }
        }

    protected val authenticationCallbackLow: FingerprintManagerCompat.AuthenticationCallback
        get() {

            return object : FingerprintManagerCompat.AuthenticationCallback() {
                override fun onAuthenticationError(errMsgId: Int, errString: CharSequence?) {
                    super.onAuthenticationError(errMsgId, errString)
                    biometricDialog.setSubMessage("Fingerprint not recognized")

                }

                override fun onAuthenticationHelp(helpMsgId: Int, helpString: CharSequence?) {
                    super.onAuthenticationHelp(helpMsgId, helpString)
                    biometricDialog.setSubMessage("Fingerprint not recognized")

                }

                override fun onAuthenticationSucceeded(result: FingerprintManagerCompat.AuthenticationResult?) {
                    super.onAuthenticationSucceeded(result)

                    updateOnAuthSuccess()
                    if (!isFinishing) {
                        biometricDialog.dismissDialog()
                    }
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    biometricDialog.setSubMessage("Fingerprint not recognized")
                }
            }
        }

    @TargetApi(Build.VERSION_CODES.M)
    protected fun initFingerprint() {
        fingerprintImg!!.setOnClickListener {
            //onClick listener for fingerprint image
            val wmBiometric = WMBiometric(this)
            //Biometric dialog needs to be passed in the context, a layout inflater, the current activity, and the callback information for fingerprint compact

            biometricDialog = BiometricDialog(layoutInflater, this, wmBiometric.cancellcationSignal)
            biometricDialog.setFinished(false)

            if (!NetUtils.isSdkVersionSupported) { //Checks if API version is Android Pie or above
                //Biometric Prompt auth; need to pass in a callback, activity, and if it's the first biometric login
                wmBiometric.biometricPromptAuth(authenticationCallback, this, false)
            } else {
                Timber.d("low version") //For API version that are below Android Pie
                wmBiometric.biometricAuth(authenticationCallbackLow) //Fingerprint compact auth; need to pass in a callback
                biometricDialog.showDialog() //custom dialog needs to be show for fingerprint compact
            }
        }
    }
}
