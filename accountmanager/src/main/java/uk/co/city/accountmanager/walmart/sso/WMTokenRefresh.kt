package uk.co.city.accountmanager.walmart.sso

import uk.co.city.core.walmart.wmsso.WMUser

/**
 */
interface WMTokenRefresh {
    fun refreshToken(user: WMUser): WMUser
}
