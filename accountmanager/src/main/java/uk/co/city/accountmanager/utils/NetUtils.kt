package uk.co.city.accountmanager.utils

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.util.Log
import android.view.WindowManager
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import timber.log.Timber
import uk.co.city.accountmanager.walmart.sso.WMLoginActivity

object NetUtils{
    fun isNetworkAvailable(context: Context): Boolean {
        var result = false

        try {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val info = cm.activeNetworkInfo
            if (info != null && info.isAvailable && info.isConnected) {
                result = true
            }
        } catch (e: Exception) {
            Timber.d( "Exception while checking if device is online:")
            e.printStackTrace()
        }

        return result
    }

    fun isHardwareSupported(context: Context): Boolean { //Will return true if the user's device has a fingerprint scanner
        var supported: Boolean? = false
        try { //Added a try catch due to some devices crashing when this method is called
            val fingerprintManager = FingerprintManagerCompat.from(context)
            supported = fingerprintManager.isHardwareDetected
        } catch (e: WindowManager.BadTokenException) {
            Timber.e( e.toString())
        }

        return supported!!
    }


    fun isFingerprintAvailable(context: Context): Boolean { //Will return true if user has a fingerprint saved on their device
        val fingerprintManager = FingerprintManagerCompat.from(context)
        return fingerprintManager.hasEnrolledFingerprints()
    }

    val isSdkVersionSupported: Boolean
        get() {
            Timber.d("API VERSION: ${Build.VERSION.SDK_INT}")
            return Build.VERSION.SDK_INT <= 28
        }

}