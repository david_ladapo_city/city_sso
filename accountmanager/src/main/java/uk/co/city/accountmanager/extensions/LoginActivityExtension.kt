package uk.co.city.accountmanager.extensions

import android.annotation.TargetApi
import android.app.AlertDialog
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.view.View
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import timber.log.Timber
import uk.co.city.accountmanager.R
import uk.co.city.accountmanager.utils.NetUtils
import uk.co.city.accountmanager.utils.NetUtils.isSdkVersionSupported
import uk.co.city.accountmanager.walmart.cty.FPAuthCallback
import uk.co.city.accountmanager.walmart.sso.*

var invalidCharacters = arrayOf(
    "å",
    "æ",
    "ā",
    "ă",
    "ą",
    "à",
    "á",
    "â",
    "ã",
    "ä",
    "ç",
    "ć",
    "č",
    "ď",
    "đ",
    "ė",
    "ę",
    "ě",
    "ĕ",
    "ə",
    "è",
    "é",
    "ê",
    "ë",
    "ē",
    "ģ",
    "ğ",
    "ı",
    "į",
    "ī",
    "ï",
    "î",
    "í",
    "ì",
    "ķ",
    "ł",
    "ľ",
    "ļ",
    "ĺ",
    "ň",
    "ņ",
    "ń",
    "ñ",
    "ⁿ",
    "œ",
    "ő",
    "ø",
    "ö",
    "õ",
    "ô",
    "ó",
    "ò",
    "ŕ",
    "ř",
    "ś",
    "š",
    "ş",
    "ß",
    "§",
    "ť",
    "ț",
    "ţ",
    "ų",
    "ű",
    "ů",
    "ū",
    "ü",
    "û",
    "ú",
    "ù",
    "ý",
    "ź",
    "ż",
    "ž",
    "¹",
    "²",
    "³",
    "⁴"
)

fun WMLoginActivity.showAlert(title: String, msg: String) {
    val builder = AlertDialog.Builder(this)
    builder.setMessage(msg).setTitle(title)
    builder.setPositiveButton(android.R.string.ok) { dialog, which -> dialog.dismiss() }
    val dialog = builder.create()
    dialog.show()
}

fun WMLoginActivity.checkValidWithNetwork(isValidChar: Boolean, valid: Boolean?, msg: String) {
    if ((!valid!!) && NetUtils.isNetworkAvailable(this) && isValidChar) {
        showAlert(getString(R.string.missingFields), msg)
    } else if (!NetUtils.isNetworkAvailable(this)) {
        showAlert(
            getString(R.string.errorinternetconn),
            getString(R.string.errorinternetconnmsg)

        )
    }
}

/**
 * Alert dialog for if the users wants to clear all biometric settings
 */
fun WMLoginActivity.showClearBiometricDialog(
    activity: AppCompatActivity,
    sharedPref: WMBiometricSharedPref,
    vararg imgs: ImageView
) {
    val builder =
        AlertDialog.Builder(activity).apply {
            setCancelable(true)
            setTitle(activity.getString(R.string.clear_sso_data))
            setMessage(activity.getString(R.string.clear_biometric_message))
            setPositiveButton(
                activity.getString(R.string.accept)
            ) { dialog, which ->
                sharedPref.setUserBiometricPref(true, activity) //first login variable will be true
                sharedPref.setUserBiometricInfo(false, activity) //user will be opted out of using biometric
                for (img in imgs) {
                    img.visibility = View.INVISIBLE
                }
            }
            setNegativeButton(android.R.string.cancel) { dialog, which -> dialog.dismiss() }

        }
    val dialog = builder.create()
    dialog.show()
}


@RequiresApi(Build.VERSION_CODES.M)
@TargetApi(Build.VERSION_CODES.M)
fun WMLoginActivity.showBiometricDialog(
    crypto: WMCryptoKeystoreImpl,
    walmart: WMLoginActivity.WalmartAuthProperties,

    sharedPref: WMBiometricSharedPref
): BiometricDialog {
    val wmBiometric = WMBiometric(this)
    //Biometric dialog needs to be passed in the context, a layout inflater, the current activity, and the callback information for fingerprint compact

    val biometricDialog =
        BiometricDialog(layoutInflater, this, wmBiometric.cancellcationSignal)
    biometricDialog.setFinished(false)

    if (!isSdkVersionSupported) {
        val bioAuthCallback = authenticationCallback(crypto, walmart,  sharedPref)
        wmBiometric.biometricPromptAuth(bioAuthCallback, this, true)
    } else {
        Timber.d("showing low version of biometric dialog")
        val fingerprintAuthCallback =
            fpAuthenticationCallback(this, walmart, sharedPref, biometricDialog)
        wmBiometric.biometricAuth(fingerprintAuthCallback)
        biometricDialog.showDialog()
    }
    return biometricDialog
}

private fun fpAuthenticationCallback(
    activity: AppCompatActivity,
    walmart: WMLoginActivity.WalmartAuthProperties,
    sharedPref: WMBiometricSharedPref, biometricDialog: BiometricDialog
): FingerprintManagerCompat.AuthenticationCallback {
    return FPAuthCallback(walmart, activity, sharedPref, biometricDialog)
}

 fun WMLoginActivity.authenticationCallback(
    crypto: WMCryptoKeystoreImpl,
    walmart: WMLoginActivity.WalmartAuthProperties,
   sharedPref: WMBiometricSharedPref
): BiometricPrompt.AuthenticationCallback {
    return object :
        BiometricPrompt.AuthenticationCallback() {
        override fun onAuthenticationError(
            errorCode: Int,
            errString: CharSequence
        ) {
            super.onAuthenticationError(
                errorCode,
                errString
            )
            finish()

        }

        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
            super.onAuthenticationSucceeded(result)
            sharedPref.setUserBiometricInfo(true, baseContext)
//Encrypts and stores the user's login information
            sharedPref.storeUserCreds(
                crypto.encrypt(walmart.userid)!!,
                crypto.encrypt(walmart.password)!!,
                walmart.domain!!,
                walmart.siteId,
                walmart.country!!,
                baseContext
            )
            finish()
        }

    }
}


enum class SSOIconCharCode private constructor(private val `val`: String) {
    StrokeCircleSolidKey("\ue905"),
    SolidCircleKey("\ue900"),
    StrokeCircleKey("\ue901"),
    User("\ue902"),
    Key("\ue903"),
    Lock("\ue904"),
    Forward("\ue607");

    fun code(): String {
        return `val`
    }
}