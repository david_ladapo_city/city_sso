package uk.co.city.ctysso.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.forgot_password.*
import uk.co.city.ctysso.R

class ForgotPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forgot_password)
        reset_password_button.setOnClickListener {
            submitAndForward()
        }
    }

    private fun submitAndForward() {
        val email = intent.getStringExtra("email")
        val intent: Intent = Intent(this, ChangePasswordActivity::class.java).apply {
            putExtra("email_address", email)

        }
        startActivity(intent)
    }

}
