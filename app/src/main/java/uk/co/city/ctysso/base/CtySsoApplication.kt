package uk.co.city.ctysso.base

import android.app.Application
import timber.log.Timber
import uk.co.city.core.NotLoggingTree
import uk.co.city.ctysso.BuildConfig

class CtySsoApplication:Application(){

    @Override
    override fun onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        else {
            Timber.plant(NotLoggingTree())
        }
    }

}